<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

$route['goadmin'] = "goadmin/login";
$route['contact/(:any)'] = "contact/index/$1";

$route['news-events'] = "news_events/index";
$route['news-events/hal/(:num)'] = "news_events/hal/$1";
$route['news-events/hal'] = "news_events";
$route['news-events/(:any)'] = "news_events/detail/$1";

$route['about/our-values'] = "about/our_values";
$route['about/our-partners'] = "about/our_partners";
$route['about/our-history'] = "about/our_history";

$route['industry-solutions'] = "industry_solutions/index";
$route['industry-solutions/(:any)'] = "industry_solutions/detail/$1";

// $route['translate_uri_dashes'] = TRUE;
// URI like '/en/about' -> use controller 'about'
// UNCOMMENT FOR INTERNATIONALIZATION
//$route['^(en|id)/(.+)$'] = "$2";

// '/en' and '/fr' URIs -> use default controller
// UNCOMMENT FOR INTERNATIONALIZATION
//$route['^(en|id)$'] = $route['default_controller'];



/* End of file routes.php */
/* Location: ./application/config/routes.php */