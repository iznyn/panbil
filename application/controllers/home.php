<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$asset = array(
					'title'	=> 'Home',
					'js'	=> array('slick/slick'), 
					'css'	=> array('../js/slick/slick','../js/slick/slick-theme'),
					'menuservices'	=> $this->db->get_where('services', array('flag' => 1 , 'services_section' => 9)),
					'menuindusties'	=> $this->db->get_where('industry', array('flag' => 1 , 'industry_section' => 8)),
					'web'	=> $this->db->get_where('setting', array('flag' => 1))->row_array()
				);
				
		$asset['banner'] = $this->db->order_by('sort','asc')->get_where('banner', array('flag' => 1))->result_array();
		$asset['arservice'] = $this->db->order_by('sort','asc')->get_where('services', array('flag' => 1, 'services_section' => 9))->result_array();
		
		$this->load->view('template/header', $asset);	
		$this->load->view('template/top');
		$this->load->view('home_view');
		$this->load->view('template/footer');
	}
} ?>