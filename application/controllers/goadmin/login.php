<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$asset = array(
					'title'	=> 'Login',
					'js'	=> array('jquery.validate.min', 'admin/login'),
					'css'	=> array(),
					'web'	=> $this->db->order_by('setting_id', 'desc')->get_where('setting', array('flag' => 1))->row_array()
				);
				
		if ($this->session->userdata('admin_login') === TRUE) $asset['title'] = 'Home';
		
		$this->load->view('admin/template/header', $asset);
		
		if ($this->session->userdata('admin_login') === FALSE) $this->load->view('admin/login_view');
		elseif ($this->session->userdata('admin_login') === TRUE)
		{
			if ($this->session->userdata('referral'))
			{
				$url = $this->session->userdata('referral');
				$this->session->unset_userdata('referral');
				redirect($url);
			}
			$this->load->view('admin/template/menu');
		}
		
		$this->load->view('admin/template/footer');
	}
	
	public function validate()
	{
		$this->load->model('model_security');
		
		if ($this->model_security->login() === TRUE)
		{
			echo 'success';
		}
		
		elseif ($this->model_security->login() === FALSE) echo 'warning';
		elseif ($this->model_security->login() === NULL) echo 'incorrect';
		
		
	}
}