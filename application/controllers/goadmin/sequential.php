<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sequential extends CI_Controller {

	public function index(){
		redirect();
	}
    
    public function delete(){
        $idList = json_decode($this->input->post('id'));
        $tableName = $this->input->post('table');
        $total = count($idList);
        $data = array(
            'flag' => 3
        );
        foreach($idList as $v1){
            $this->db->where('unique_id', $v1)->update($tableName, $data);
        }
        $return['result'] = 'success';
        $return = array(
            'result' => 'success',
            'count' => $total
        );
        
    }
    
}