<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function flag()
	{
		$this->model_process->flag();
	}
	
	public function delete()
	{
		$this->model_process->delete();
	}
	
	public function delete_image()
	{
		$this->model_process->delete_image();
	}
}