<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_contact extends CI_Model {
	
	var $table = 'message';
	
	public function insert()
	{
		$language = language()->result_array();		
		$unique_id = unique_id($this->table);
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'		=> $unique_id,
					'message_name'	=> $this->input->post('message_name'),
					'message_subject'=> $this->input->post('message_subject'),
					'message_email'=> $this->input->post('message_email'),
					'message_content'=> $this->input->post('message_content'),
					'flag'			=> "1",
					'flag_memo'		=> ""
				);
			
			$this->db->insert($this->table, $data);
		}
		
		// Query for log :)
		$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $data['unique_id']))->row_array();
		
		action_log('ADD', $this->table, $row['unique_id'], $row[$this->table. '_name'], 'ADDED ' . $this->table. ' ( ' . $row[$this->table. '_name'] . ' ) ');
	}
}