<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_industry extends CI_Model {
	
	var $table = 'industry';
	
	public function insert()
	{
		// Upload Image
		$file = file_upload('industry_image', 'images/industry', FALSE);
		$file2 = file_upload('industry_image_hover', 'images/industry', FALSE);
		$file3 = file_upload('industry_image_content', 'images/industry', FALSE);
		
		if ($file)
		{
			$image = image_resize($file, 75, 75);
			
			$file_name = $image['file_name'];
		}
		else $file_name = '';
		
		if ($file2)
		{
			$image2 = image_resize($file2, 75, 75);
			
			$file_name2 = $image2['file_name'];
		}
		else $file_name2 = '';
		
		if ($file3)
		{
			//$image3 = image_resize($file3, 75, 75);
			
			$file_name3 = $file3['file_name'];
		}
		else $file_name3 = '';
		
		$language = language()->result_array();
		$unique_id = unique_id($this->table);
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $unique_id,
					'language_id'		=> $lang_data['language_id'],
					'industry_name'		=> $this->input->post('industry_name_' . $lang_data['language_id']),
					'seo_url'			=> url_title(trim($this->input->post('industry_name_' . $language[0]['language_id'])), 'underscore', TRUE),
					'industry_head'		=> $this->input->post('industry_head_' . $lang_data['language_id']),
					'industry_head2'		=> $this->input->post('industry_head2_' . $lang_data['language_id']),
					'industry_image'		=> $file_name,
					'industry_image_hover'		=> $file_name2,
					'industry_image_content'		=> $file_name3,
					'industry_content'	=> $this->input->post('industry_content_' . $lang_data['language_id']),
					'industry_section'	=> $this->input->post('industry_section'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			$this->db->insert($this->table, $data);
		}
			
		// Query for log :)
		$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $data['unique_id']))->row_array();
		
		action_log('ADD', $this->table, $row['unique_id'], $row[$this->table. '_name'], 'ADDED ' . $this->table. ' ( ' . $row[$this->table. '_name'] . ' ) ');
	}
	
	public function update()
	{
		// Upload Image
		$file = file_upload('industry_image', 'images/industry', FALSE);
		$file2 = file_upload('industry_image_hover', 'images/industry', FALSE);
		$file3 = file_upload('industry_image_content', 'images/industry', FALSE);
		
		if ($file)
		{
			$image = image_resize($file, 75, 75);
			$file_name = $image['file_name'];
		}
		else
		{
			$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $this->input->post('id')))->result_array();
			$file_name = $row[0]['industry_image'];
		}
		
		if ($file2)
		{
			$image2 = image_resize($file, 75, 75);
			$file_name2 = $image2['file_name'];
		}
		else
		{
			$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $this->input->post('id')))->result_array();
			$file_name2 = $row[0]['industry_image_hover'];
		}
		
		if ($file3)
		{
			//$image3 = image_resize($file3, 75, 75);
			$file_name3 = $file3['file_name'];
		}
		else
		{
			$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $this->input->post('id')))->result_array();
			$file_name3 = $row[0]['industry_image_content'];
		}
		
		$language = language()->result_array();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $this->input->post('id'),
					'language_id'		=> $lang_data['language_id'],
					'industry_name'		=> $this->input->post('industry_name_' . $lang_data['language_id']),
					'seo_url'			=> url_title(trim($this->input->post('industry_name_' . $language[0]['language_id'])), 'underscore', TRUE),
					'industry_head'		=> $this->input->post('industry_head_' . $lang_data['language_id']),
					'industry_head2'		=> $this->input->post('industry_head2_' . $lang_data['language_id']),
					'industry_image'		=> $file_name,
					'industry_image_hover'		=> $file_name2,
					'industry_image_content'		=> $file_name3,
					'industry_content'	=> $this->input->post('industry_content_' . $lang_data['language_id']),
					'industry_section'	=> $this->input->post('industry_section'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			// Pertama cek, language itu udah ada ato blom
			$exist = $this->db->select($this->table . '_id')->where(array('language_id' => $lang_data['language_id'], 'unique_id' => $this->input->post('id')))->get($this->table)->row_array();
		
			// Kalo ada, kita update aja :)
			$this->db->where($this->table . '_id', $exist[$this->table . '_id']);
			$this->db->update($this->table, $data);
		}
		
		$row = $this->db->get_where($this->table, array('unique_id' => $this->input->post('id')))->row_array();
		if($row['flag'] == 3) {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'DELETED ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');		
		} else {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'MODIFY ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');
		}
	}
}