<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_search extends CI_Model {

	var $table = 'news';
		
	public function record_count() {
		$query = $this->db->get_where('news', array('flag' => 1, 'news_content like' => "%$_POST[search]%"))->num_rows();
        return $query;
    }

    public function fetch_news($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by('news_start', 'desc')->get_where('news', array('flag' => 1, 'news_content like' => "%$_POST[search]%"));

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
}