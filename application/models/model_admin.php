<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_admin extends CI_Model {
	
	var $table = 'admin';
	
	public function insert()
	{
		$data = array(
				'unique_id'			=> unique_id($this->table),
				'admin_name'		=> $this->input->post('admin_name'),
				'admin_username'	=> $this->input->post('admin_username'),
				'admin_password'	=> md5($this->input->post('admin_password')),
                'admin_email'		=> $this->input->post('admin_email'),
                'admin_mobile'		=> $this->input->post('admin_mobile'),
				'admin_privilege'	=> $this->input->post('admin_privilege'),
				'module_list'		=> '',
				'access'			=> '',
				'flag'				=> $this->input->post('flag'),
				'flag_memo'			=> $this->input->post('flag_memo')
			);
			
		
		// For custom privilege
		if ($this->input->post('admin_privilege') == 3)
		{
			// Get all module list
			$modules = $this->db->get_where('module', array('flag !=' => 3, 'module_parent !=' => 0));
			
			foreach ($modules->result_array() as $item)
				$module_list[$item['module_id']] = $this->input->post($item['module_id']);
			
			$data['module_list'] = implode(',', array_keys($module_list));
			$data['access'] = implode(',', array_values($module_list));
		}
		
		$this->db->insert($this->table, $data);
		
		// Query for log :)
		$row = $this->db->get_where($this->table, array($this->table . '_id' => $this->db->insert_id()))->row_array();
		
		action_log('ADD', $this->table, $row[$this->table . '_id'], $row[$this->table. '_name'], 'ADDED ' . $this->table. ' ( ' . $row[$this->table. '_name'] . ' ) ');
	}
	
	public function update()
	{
		$data = array(
				'admin_privilege'	=> $this->input->post('admin_privilege'),
				'flag'				=> $this->input->post('flag'),
				'flag_memo'			=> $this->input->post('flag_memo'),
                'admin_email'		=> $this->input->post('admin_email'),
                'admin_mobile'		=> $this->input->post('admin_mobile'),
                'admin_name'		=> $this->input->post('admin_name'),
				'module_list'		=> '',
				'access'			=> ''
			);
		
		if ($this->input->post('admin_re_password')) $data['admin_password'] = md5($this->input->post('admin_re_password'));
		
		if ($this->input->post('admin_privilege') == 3)
		{
			// Get all module list
			$modules = $this->db->get_where('module', array('flag !=' => 3, 'module_parent !=' => 0));
			
			foreach ($modules->result_array() as $item)
				$module_list[$item['module_id']] = $this->input->post($item['module_id']);
			
			$data['module_list'] = implode(',', array_keys($module_list));
			$data['access'] = implode(',', array_values($module_list));
		}
		
		$this->db->where('unique_id', $this->input->post('id'));
		$this->db->update($this->table, $data);
		
		// Query for log :)
		$row = $this->db->get_where($this->table, array('unique_id' => $this->input->post('id')))->row_array();
		
		action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'MODIFY ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');
	}
}