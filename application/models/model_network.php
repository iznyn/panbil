<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_network extends CI_Model {
	
	var $table = 'network';
	
	public function insert()
	{
		$language = language()->result_array();
		$unique_id = unique_id($this->table);
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $unique_id,
					'language_id'		=> $lang_data['language_id'],
					'sort'		=> $this->input->post('sort_' . $lang_data['language_id']),
					'network_name'		=> $this->input->post('network_name_' . $lang_data['language_id']),
					'network_description'	=> $this->input->post('network_description_' . $lang_data['language_id']),
					'network_address'	=> $this->input->post('network_address_' . $lang_data['language_id']),
					'network_city'	=> $this->input->post('network_city_' . $lang_data['language_id']),
					'network_postcode'	=> $this->input->post('network_postcode_' . $lang_data['language_id']),
					'network_phone'	=> $this->input->post('network_phone_' . $lang_data['language_id']),
					'network_fax'	=> $this->input->post('network_fax_' . $lang_data['language_id']),
					'network_gmap'	=> $this->input->post('network_gmap_' . $lang_data['language_id']),
					'network_location'	=> $this->input->post('network_location'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			$this->db->insert($this->table, $data);
		}
			
		// Query for log :)
		$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $data['unique_id']))->row_array();
		
		action_log('ADD', $this->table, $row['unique_id'], $row[$this->table. '_name'], 'ADDED ' . $this->table. ' ( ' . $row[$this->table. '_name'] . ' ) ');
	}
	
	public function update()
	{
		$language = language()->result_array();
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $this->input->post('id'),
					'language_id'		=> $lang_data['language_id'],
					'sort'		=> $this->input->post('sort_' . $lang_data['language_id']),
					'network_name'		=> $this->input->post('network_name_' . $lang_data['language_id']),
					'network_description'	=> $this->input->post('network_description_' . $lang_data['language_id']),
					'network_address'	=> $this->input->post('network_address_' . $lang_data['language_id']),
					'network_city'	=> $this->input->post('network_city_' . $lang_data['language_id']),
					'network_postcode'	=> $this->input->post('network_postcode_' . $lang_data['language_id']),
					'network_phone'	=> $this->input->post('network_phone_' . $lang_data['language_id']),
					'network_fax'	=> $this->input->post('network_fax_' . $lang_data['language_id']),
					'network_gmap'	=> $this->input->post('network_gmap_' . $lang_data['language_id']),
					'network_location'	=> $this->input->post('network_location'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			// Pertama cek, language itu udah ada ato blom
			$exist = $this->db->select($this->table . '_id')->where(array('language_id' => $lang_data['language_id'], 'unique_id' => $this->input->post('id')))->get($this->table)->row_array();
		
			// Kalo ada, kita update aja :)
			$this->db->where($this->table . '_id', $exist[$this->table . '_id']);
			$this->db->update($this->table, $data);
		}
		
		$row = $this->db->get_where($this->table, array('unique_id' => $this->input->post('id')))->row_array();
		if($row['flag'] == 3) {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'DELETED ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');		
		} else {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'MODIFY ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');
		}
	}
}