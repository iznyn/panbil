<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_security extends CI_Model {
	
	public function login()
	{
		$condition = array(
						'admin_username'	=> $this->input->post('username'),
						'admin_password'	=> md5($this->input->post('password')),
						'flag !='		=> 3
					);
					
		$query = $this->db->get_where('admin', $condition)->row_array();
		
		if (empty($query)) return NULL;
		else
		{
			if ($query['flag'] == 1)
			{
				// Set admin session data
				$sess_data = array(
								'admin_login'	=> TRUE,
								'admin_name'	=> $query['admin_name'],
								'admin_id'		=> $query['admin_id']
							);
							
				$this->session->set_userdata($sess_data);
				action_log('LOGIN', 'admin', $query['admin_id'], $query['admin_name'], 'Successful Login');
				
				return TRUE;
			}
			elseif ($query['flag'] == 2)
			{
				action_log('LOGIN', 'admin', $query['admin_id'], $query['admin_name'], 'Login Failed (Inactive)');
				return FALSE;
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */