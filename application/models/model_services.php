<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_services extends CI_Model {
	
	var $table = 'services';
	
	public function insert()
	{
		$language = language()->result_array();
		$unique_id = unique_id($this->table);
		
		$pictname = url_title(trim($this->input->post('services_name_' . $language[0]['language_id'])), 'underscore', TRUE);
		
		$file = file_upload_new($pictname, 'services_image', 'images/services', FALSE);
		$file2 = file_upload_new($pictname, 'services_image', 'images/services', FALSE);
		//$bigimages = file_upload('services_image_content', 'images/services', FALSE);		
		
		if ($file)
		{
			if($this->input->post('services_section')==9){
				$image = image_resize($file, 50, 50);
				$image2 = image_resize($file2, 105, 105);
			}
			
			$file_name = $image['file_name'];
			$filegabungan = $file_name."||".$file_name2;
		}
		
		else $filegabungan = '';
		
		if ($file2)
		{
			$file_name2 = $image2['file_name'];
			$filegabungan = $file_name."||".$file_name2;
		}
		
		else $filegabungan = '';
		
		/*
		if ($bigimages)
		{
			$imagecontent = $bigimages['file_name'];
		}
		
		else $imagecontent = '';
		*/
		
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $unique_id,
					'language_id'		=> $lang_data['language_id'],
					'services_name'		=> $this->input->post('services_name_' . $lang_data['language_id']),
					'seo_url'			=> url_title(trim($this->input->post('services_name_' . $language[0]['language_id'])), 'underscore', TRUE),
					'services_head'		=> $this->input->post('services_head_' . $lang_data['language_id']),
					'services_image'		=> $filegabungan,
					//'services_image_content'		=> $imagecontent,
					'services_content'	=> $this->input->post('services_content_' . $lang_data['language_id']),
					'services_section'	=> $this->input->post('services_section'),
					'services_link'		=> $this->input->post('services_link'),
					'sort'				=> $this->input->post('sort'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			$this->db->insert($this->table, $data);
		}
			
		// Query for log :)
		$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $data['unique_id']))->row_array();
		
		action_log('ADD', $this->table, $row['unique_id'], $row[$this->table. '_name'], 'ADDED ' . $this->table. ' ( ' . $row[$this->table. '_name'] . ' ) ');
	}
	
	public function update()
	{
		// Upload Image
		$language = language()->result_array();
		$pictname = url_title(trim($this->input->post('services_name_' . $language[0]['language_id'])), 'underscore', TRUE);
		
		$file = file_upload_new($pictname, 'services_image', 'images/services', FALSE);
		$file2 = file_upload_new($pictname, 'services_image', 'images/services', FALSE);
		//$bigimages = file_upload('services_image_content', 'images/services', FALSE);
		
		if ($file)
		{
			if($this->input->post('services_section')==9){
				$image = image_resize($file, 50, 50);
				$image2 = image_resize($file2, 105, 105);
			}
			
			$file_name = $image['file_name'];
			$file_name2 = $image2['file_name'];
			$filegabungan = $file_name."||".$file_name2;
		}
		else
		{
			$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $this->input->post('id')))->result_array();
			$filegabungan = $row[0]['services_image'];
		}
		/*
		if ($bigimages)
		{
			$imagecontent = $bigimages['file_name'];
		}
		else
		{
			$row = $this->db->order_by($this->table . '_id', 'asc')->get_where($this->table, array('unique_id' => $this->input->post('id')))->result_array();
			$imagecontent = $row[0]['services_image_content'];
		}
		*/
		foreach ($language as $lang_data)
		{
			$data = array(
					'unique_id'			=> $this->input->post('id'),
					'language_id'		=> $lang_data['language_id'],
					'services_name'		=> $this->input->post('services_name_' . $lang_data['language_id']),
					'seo_url'			=> url_title(trim($this->input->post('services_name_' . $language[0]['language_id'])), 'underscore', TRUE),
					'services_head'		=> $this->input->post('services_head_' . $lang_data['language_id']),
					'services_image'		=> $filegabungan,
					//'services_image_content'		=> $imagecontent,
					'services_content'	=> $this->input->post('services_content_' . $lang_data['language_id']),
					'services_section'	=> $this->input->post('services_section'),
					'services_link'		=> $this->input->post('services_link'),
					'sort'				=> $this->input->post('sort'),
					'flag'				=> $this->input->post('flag'),
					'flag_memo'			=> $this->input->post('flag_memo')
				);
				
			// Pertama cek, language itu udah ada ato blom
			$exist = $this->db->select($this->table . '_id')->where(array('language_id' => $lang_data['language_id'], 'unique_id' => $this->input->post('id')))->get($this->table)->row_array();
		
			// Kalo ada, kita update aja :)
			$this->db->where($this->table . '_id', $exist[$this->table . '_id']);
			$this->db->update($this->table, $data);
		}
		
		$row = $this->db->get_where($this->table, array('unique_id' => $this->input->post('id')))->row_array();
		if($row['flag'] == 3) {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'DELETED ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');		
		} else {
			action_log('UPDATE', $this->table, $row['unique_id'], $row[$this->table . '_name'], 'MODIFY ' . $this->table . ' ( ' . $row[$this->table . '_name'] . ' ) ');
		}
	}
}

/*
if(!empty($_FILES)){
			$post = $this->input->post();
			$content_pict = array();
			$i=0;
			
			foreach ($_FILES["service_banner_image"]["name"] as $key => $value) {
				if(!empty($value)){
					// Upload Image
					$multiple_files = RFw_multi_upload('service_banner_image', 'images/services');
					
					if ($multiple_files){
						// $image = image_resize($file, $this->image_width, $this->image_height);
						foreach ($multiple_files as $k => $v) {

							//image_watermark($v, $this->image_watermark);
							//RFw_crop($v, $this->banner_image_width, $this->banner_image_height);
							$content_pict[] = $v['file_name'];
						}
					}
				}else{
					if(!empty($post["service_banner_image"])){
						if(!empty($post["service_banner_image"][$key])) $content_pict[] = $post["service_banner_image"][$key];
						else $content_pict[] = '';
					}
				}
				
				//var_dump($content_pict[0]);die;
				
				$data = array(
						'service_banner_services'    => $unique_id,
						'service_banner_image'    => $content_pict[$i],
						'sort'				=> $_POST['service_banner_sort'][$i],
						'flag'				=> $this->input->post('flag'),
						'flag_memo'			=> $this->input->post('flag_memo')
					);

				
				$this->db->insert('service_banner', $data);
				$i++;
			}
		}
*/