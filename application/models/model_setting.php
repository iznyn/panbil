<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_setting extends CI_Model {
	
	var $table = 'setting';
	
	public function update()
	{
		// Get previous row
		$active_row = $this->db->order_by('setting_id', 'desc')->get_where('setting', array('flag' => 1))->row_array();
		
		// Update to flag = 2
		$this->db->where('setting_id', $active_row['setting_id']);
		$this->db->update('setting', array('flag' => 2));
		
		// Upload image!
		$file = file_upload('setting_web_logo', 'images', FALSE);
		
		if ($file)
		{
			$image = image_resize($file, 200, 200);
			$file_name = $image['file_name'];
		}
		else $file_name = $active_row['setting_web_logo'];
		
		// Upload favicon!
		$icon = file_upload('setting_favicon', '');
		
		if ($icon) $icon_name = $icon['file_name'];
		else $icon_name = $active_row['setting_favicon'];
			
		
		$data = array(
				'setting_name'		=> $this->input->post('setting_name'),
				'setting_address'	=> $this->input->post('setting_address'),
				'setting_working_time'	=> $this->input->post('setting_working_time'),
				'setting_country'	=> $this->input->post('setting_country'),
				'setting_city'		=> $this->input->post('setting_city'),
				'setting_postcode'	=> $this->input->post('setting_postcode'),
				'setting_phone'		=> $this->input->post('setting_phone'),
				'setting_mobile'	=> $this->input->post('setting_mobile'),
				'setting_bb_pin'	=> $this->input->post('setting_bb_pin'),
				'setting_fax'		=> $this->input->post('setting_fax'),
				'setting_email'		=> $this->input->post('setting_email'),
				'setting_ym'		=> $this->input->post('setting_ym'),
				'setting_msn'		=> $this->input->post('setting_msn'),
				'setting_facebook'	=> $this->input->post('setting_facebook'),
				'setting_twitter'	=> $this->input->post('setting_twitter'),
				'setting_google_map'=> $this->input->post('setting_google_map'),
				'setting_google_analytics'	=> $this->input->post('setting_google_analytics'),
				'setting_web_title'	=> $this->input->post('setting_web_title'),
				'setting_web_motto'	=> $this->input->post('setting_web_motto'),
				'setting_web_logo'	=> $file_name,
				'setting_favicon'	=> $icon_name,	
				'setting_meta_desc'	=> $this->input->post('setting_meta_desc'),
				'setting_meta_key'	=> $this->input->post('setting_meta_key'),
				'flag'				=> 1,
				'flag_memo'			=> $this->input->post('flag_memo')
			);
		
		$this->db->insert($this->table, $data);
	}
}