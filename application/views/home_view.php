<div id="center">
	
	<div class="bg-one">
		<div class="banner">
			<div class="itemBanner">
				<div class="imgBanner" style="background-image: url('images/public/bg/bg-1.jpg');"></div>
				<div class="titleBanner">						
					<div class="container">
						<div class="row">
							<h1>Integrated Solution <br />for Your Investment</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="itemBanner">
				<div class="imgBanner" style="background-image: url('images/public/bg/bg-2.jpg');"></div>
				<div class="titleBanner">						
					<div class="container">
						<div class="row">
							<h1>We committed to providing world <br />class manufacturing facilities</h1> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-one-deco" style="display:none;">
		<img src="images/public/bg/bg-ornamen-top.png" alt="">
	</div>

	<div class="bg-two">		
		<div class="glanceAbout">
			<div class="container">
				<div class="row">
					<div class="ctGlanceAbout">
						<p>
							Panbil Industrial Estate is committed to providing world class manufacturing facilities, <br />excellent infrastructure and fully integrated support services to the investors. We also <br />aim to create a vibrant and dynamic environment where people and companies can grow <br />and thrive together.
						</p>
					</div>
					<div class="text-center">					
						<a href="javascript:;" class="btn btn-trans">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="bg-three">
		<div class="glanceCompetitive">
			<div class="container">
				<div class="row">
					<div class="ctGlanceCompetitive">
						<h1>Competitive Edge</h1>
						<div class="descCompetitive">
							<p>
								There are many advantages to choose Panbil as your manufacturing destination. All sites are planned, <br />
								developed, well managed and specifically designed to meet industrial needs. Full service land is <br />
								available for customised, self-built industrial and standard factory buildings.
							</p>
						</div>
						<div class="wrItem listCompetitive">
							<div class="col-4">
								<div class="itemList"><h6>Strategic&nbsp;Location</h6></div>
							</div>
							<div class="col-4">
								<div class="itemList even"><h6>Fully Integrated and Self Contained</h6></div>
							</div>
							<div class="col-4">
								<div class="itemList"><h6>Comprehensive And Secure Infrastructure</h6></div>
							</div>
							<div class="col-4">
								<div class="itemList even"><h6>More Than Just  A Workplace</h6></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-deco-top"></div>

	 
	<div class="bg-four">
		<div class="bg-triangle">
			<div class="container">
				<div class="row">
					<h1>Products</h1>
					<div class="descProd">
						<span>Residential Properti</span>
						<p>
							Savor the rich magic of Batam Island; Villa Panbil is indulge in a private world of luxury with characteristic features. Set against the backdrop of the beauty of Duriangkang lake view and pockets of quiet corners enclosed by serene nature forest and landscaping, Villa Panbil offers you both the unique nature experience and all the comports of a connected and cosmopolitan lifestyle. 
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="wrDef wrProduct">
			<div class="col-2 itProd">
				<a href="javascript:;">					
					<div class="itemProduct">
						<img src="<?php echo base_url('images/public/products/products-1.jpg');?>" alt="Product" />
						<h1>Industrial Property</h1>
					</div>
				</a>
			</div>
			<div class="col-2 itProd">
				<a href="javascript:;">					
					<div class="itemProduct">
						<img src="<?php echo base_url('images/public/products/products-3.jpg');?>" alt="Product" />
						<h1>COMMERCIAL PROPERTY</h1>
					</div> 
				</a> 
			</div>
			<div class="col-2 itProd">
				<a href="javascript:;">					
					<div class="itemProduct">
						<img src="<?php echo base_url('images/public/products/products-2.jpg');?>" alt="Product" />
						<h1>condotel</h1>
					</div>
				</a>
			</div>
			<div class="col-2 itProd">
				<a href="javascript:;">					
					<div class="itemProduct">
						<img src="<?php echo base_url('images/public/products/products-4.jpg');?>" alt="Product" />
						<h1>service apartment</h1>
					</div>
				</a> 
			</div> 
		</div>
	</div>


	<div class="bg-five">
		<div class="glanceService">
			<div class="container">
				<div class="row">
					<h1>Services</h1>
					<div class="ctGlanceService wrItem">
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-1.png');?>" alt="Services" />
									<h6>Licenses and Permits for Import/Export</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-2.png');?>" alt="Services" />
									<h6>Foreign investment application</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-3.png');?>" alt="Services" />
									<h6>Employment and residence permit</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-4.png');?>" alt="Services" />
									<h6>Business visa</h6>
								</div>
							</a>
						</div>					
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-5.png');?>" alt="Services" />
									<h6>Custom and immigration clearance assistance</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-6.png');?>" alt="Services" />
									<h6>Manpower recruitment</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-7.png');?>" alt="Services" />
									<h6>Bank loan services</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-8.png');?>" alt="Services" />
									<h6>Logistic and warehousing services</h6>
								</div>
							</a>
						</div>
						<div class="col-5">
							<a href="javascript:;">
								<div class="itemService">
									<img src="<?php echo base_url('images/public/services/icon-9.png');?>" alt="Services" />
									<h6>accomodation for tenants</h6>
								</div>
							</a>
						</div>
					</div>
					<div class="text-center">
						<a href="javascript:;" class="btn btn-trans-white btn-capitalize">Explore All</a>
					</div>
				</div>
			</div>
		</div>	
	</div> 


	<div class="bg-six">
		<div class="glanceNews">
			<div class="container">
				<div class="row">
					<h1>Recent News</h1>
					<div class="ctGlanceNews">
						<div class="popularNews">
							<a href="javascript:;">
								<div class="wrItem">
									<div class="col-2">
										<div class="covPopImg">
											<img src="<?php echo base_url('images/public/img-news.png');?>" alt="News" />
										</div>
									</div>
									<div class="col-2">
										<div class="covPopDesc">
											<h2>Maecenas mollis eros ex, sit amet pharet imperdi.</h2>
											<span>26 August, 2015</span>
											<p>
												Vivamus nec dolor vel nisl celerisqu volutpat non ut sapien. Proin sed lestie sczcelerisqu. Donec tempor iaculis ultricies. Phasellus et orci ultricies, interdum odio maximus, efficitur metus. Mauris id arcu a urna blandit 
											</p>
										</div>
									</div>
								</div>
							</a>
						</div>


						<div class="listPopNews">
							<div class="wrItem">
								<div class="col-4">
									<a href="javascript:;">
										<div class="itemListPopNews">
											<h5>Maecenas mollis eros ex, sit amet pharet imperdi.</h5>
											<span>26 August, 2015</span>
											<div class="itemListDesc">
												<p>
													Vivamus nec dolor vel nisl celerisqu volutpat non ut sapien. Proin sed lestie sczcelerisqu.
												</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-4">
									<a href="javascript:;">
										<div class="itemListPopNews">
											<h5>Maecenas mollis eros ex, sit amet pharet imperdi.</h5>
											<span>26 August, 2015</span>
											<div class="itemListDesc">
												<p>
													Vivamus nec dolor vel nisl celerisqu volutpat non ut sapien. Proin sed lestie sczcelerisqu.
												</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-4">
									<a href="javascript:;">
										<div class="itemListPopNews">
											<h5>Maecenas mollis eros ex, sit amet pharet imperdi.</h5>
											<span>26 August, 2015</span>
											<div class="itemListDesc">
												<p>
													Vivamus nec dolor vel nisl celerisqu volutpat non ut sapien. Proin sed lestie sczcelerisqu.
												</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-4">
									<a href="javascript:;">
										<div class="itemListPopNews">
											<h5>Maecenas mollis eros ex, sit amet pharet imperdi.</h5>
											<span>26 August, 2015</span>
											<div class="itemListDesc">
												<p>
													Vivamus nec dolor vel nisl celerisqu volutpat non ut sapien. Proin sed lestie sczcelerisqu.
												</p>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>

						<div class="text-center">
							<a href="javascript:;" class="btn btn-borderred">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="bg-seven">
		<div class="ornamen-left"></div>
		<div class="ornamen-right"></div>

		<div class="glancePartners">
			<div class="container">
				<div class="row">
					
					<h1>Partner</h1>
					
					<div class="descPartners">
						<p>
							Panbil is the business partner for investors who seek to achieve maximum global competitiveness and investment returns. Successful operations and its economic contributions has placed Panbil as one of the top industrial estates on Batam. Panbil offers a sustainable environment for businesses from all over the world to grow and excel together.
						</p>
					</div>

					<div class="listPartners wrItem">
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-1.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-2.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-3.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-4.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-5.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-6.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-7.png');?>" alt="Partners" />
							</div>
						</div>
						<div class="col-4">
							<div class="itemPartners">
								<img src="<?php echo base_url('images/public/partners/partners-8.png');?>" alt="Partners" />
							</div>
						</div>
					</div>

					<div class="text-center">
						<a href="javascript:;" class="btn btn btn-borderred">See All</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bg-eight">
		<div class="glanceContact">
			<div class="container">
				<div class="row">
					<h1>Panbil Industrial Estate</h1>

					<div class="ctContact">
						<div class="infoContact wrItem">
							<div class="col-2">
								<div class="itemInfo">
									<p>Panbil Plaza <br />
									Jalan Ahmad Yani,  <br />
									Muka Kuning - Batam,  <br />
									Indonesia, 29433</p>
								</div>
							</div>
							<div class="col-2">
								<div class="itemInfo">
									<p>
										Phone <a href="tel:+62 778 371000">+62 778 371000</a>
									</p>
									<p>
										Fax <a href="tel:+62 778 371100">+62 778 371100</a>
									</p>
									<br />
									<p>
										<a href="mailto:info@panbil.co.id">info@panbil.co.id</a>
									</p>
								</div>
							</div>
						</div>

						<div class="tagContact">
							<p>We appreciate your comments, feedback and enquiries!</p>
						</div>
						<div class="text-center">
							<a href="javascript:;" class="btn btn-trans-white btn-capitalize">Send Us Message</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(function(){
		$('.banner').slick({
			slidesToShow: 1,
			fade: true,
			autoplay: true,
			arrows: false,
			dots: false,
			draggable: false,
			autoplaySpeed: 5000,
		})
	});
</script>