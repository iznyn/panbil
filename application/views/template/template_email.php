<div style="width:90%; border:5px solid black;">

    <div style="background-color:white; padding:15px; border-bottom:1px dashed #e0dae0;">
        <img src="<?php echo base_url() . 'images/public/logo.png'; ?>" style="float:left;" />
        <div style="clear:both;"></div>
    </div>
    
    <div style="padding:15px;">
        <?php echo $title; ?>
        
        <?php echo $message; ?>
        
        <?php if($link) { ?>
            <div style="text-align:center; margin-top:30px; margin-bottom:30px;">
                <a style="background-color:#1b1c20; color:white; padding:10px 30px; text-decoration:none;" href="<?php echo $link; ?>">
                    <?php echo $link_title; ?>
                </a>
            </div>
        <?php } ?>
        
        
        <div style="text-align:center; padding-top:20px; margin-top:50px; border-top:1px dashed #e0dae0;">
    
            <?php if($web['setting_address']) { ?>
                <div style="display:inline-block; width:30%; padding:1% 1% 1% 0%; vertical-align:top;">
                    <h4 style="text-align:left; margin:0px; font-size:14px;">
                        <!--<img style="display:inline-block;" src="<?php echo base_url() . 'images/public/icon-addresscontact.png'; ?>" />-->
                        <strong style="display:inline-block; margin-top:-26px;">Address</strong>
                    </h4>
                    <p style="text-align:left; font-size:12px; margin:10px 0px 0px 0px; line-height:1.5;"><?php echo nl2br($web['setting_address']); ?> <?php echo $web['setting_city']; ?><?php echo ($web['setting_city'] && ($web['setting_postcode'] || $web['setting_country']))?'<br />':''; ?><?php echo $web['setting_postcode']; ?> <?php echo $web['setting_country']; ?>.</p>
                </div>
            <?php } ?>
            
            <?php if($web['setting_phone']) { ?>
                <div style="display:inline-block; width:17%; padding:1%; vertical-align:top;">
                    <h4 style="text-align:left; margin:0px; font-size:14px;">
                        <!--<img style="display:inline-block;" src="<?php echo base_url() . 'images/public/icon-telpcontact.png'; ?>" />-->
                        <strong style="display:inline-block; margin-top:-26px;">Telephone</strong>
                    </h4>
                    <p style="text-align:left; font-size:12px; margin:10px 0px 0px 0px; line-height:1.5;"><a href="tel:<?php echo $web['setting_phone']; ?>"><?php echo $web['setting_phone']; ?></a></p>
                </div>
            <?php } ?>
            
            <?php if($web['setting_mobile']) { ?>
                <div style="display:inline-block; width:17%; padding:1%; vertical-align:top;">
                    <h4 style="text-align:left; margin:0px; font-size:14px;">
                        <!--<img style="display:inline-block;" src="<?php echo base_url() . 'images/public/icon-telpcontact.png'; ?>" />-->
                        <strong style="display:inline-block; margin-top:-27px;">Mobile Phone</strong>
                    </h4>
                    <p style="text-align:left; font-size:12px; margin:10px 0px 0px 0px; line-height:1.5;">
                    <?php
                        $arraymobile = explode(" / ", $web['setting_mobile']);
                        $encodemobile = json_encode($arraymobile);
                        $decodemobile = json_decode($encodemobile);
                        foreach($decodemobile as $key => $mobile) {
                            $slash = ($key >= 1)?' / ':'';
                            echo $slash . '<a href="tel:' . $mobile . '">' . $mobile . '</a>';
                        }
                    ?>
                    </p>
                </div>
            <?php } ?>
            
            <?php if($web['setting_email']) { ?>
                <div style="display:inline-block; width:20%; padding:1% 0% 1% 1%; vertical-align:top;">
                    <h4 style="text-align:left; margin:0px; font-size:14px;">
                        <!--<img style="display:inline-block;" src="<?php echo base_url() . 'images/public/icon-emailcontact.png'; ?>" />-->
                        <strong style="display:inline-block; margin-top:-26px;">Email</strong>
                    </h4>
                    <p style="text-align:left; font-size:12px; margin:10px 0px 0px 0px; line-height:1.5;"><a class="read-more" href="mailto:<?php echo $web['setting_email']; ?>"><?php echo $web['setting_email']; ?></a></p>
                </div>
            <?php } ?>
            
        </div>

        
        
    </div>

</div>
<style type="text/css">
* { padding:0px; margin:0px; font-family: sans-serif; color: black; }
</style>