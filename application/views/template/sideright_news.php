<!-- <h4 class="headSide">Follow <span class="font-bold">Us</span></h4>
<ul class="followus">
	<li><a href="http://www.facebook.com/<?php //echo $web['setting_facebook']; ?>"><span class="ic-follow-fb"></span></a></li>
	<li><a href="http://www.twitter.com/<?php //echo $web['setting_twitter']; ?>"><span class="ic-follow-tw"></span></a></li>
	<li><a href="javascript:;"><span class="ic-follow-gp"></span></a></li>
</ul> -->

<div class="popularNews margintop-0">
	<h4 class="headSide">Popular <span class="font-bold">News</span></h4>
	<?php foreach($newspopular->result_array() as $item)
	{
		$rawmy = explode("-",$item['news_start']);
		$rawdate = explode(" ",$rawmy[2]);
	?>
	<div class="itemPopularNews">
		<a href="<?php echo site_url('news-events/')."/".$item['seo_url'];?>" class="pnimg"> <!--class="pnimg"-->
			<img src="<?php echo base_url('images/news/')."/".$item['news_image'];?>" alt="<?php echo $item['news_name']; ?>">
		</a>
		<div class="descPopular">			
			<h5><a href="<?php echo site_url('news-events/')."/".$item['seo_url'];?>"><?php echo $item['news_name']; ?></a></h5>
			<?php echo substr($item['news_content'],0,160)."..."; ?>
		</div>
		<div class="panelPopular">
			<div class="partPanelPopular">
				<span><?php echo date("M",mktime(0,0,0,$rawmy[1],$rawdate[0],$rawmy[0]))." ".$rawdate[0].", ".$rawmy[0];?></span>
			</div>
			<div class="partPanelPopular">
				<a href="<?php echo site_url('news-events/')."/".$item['seo_url'];?>">Read More</a>
			</div>
		</div>
	</div>
	<?php
	}
	
	if($this->uri->rsegment(3) != ""){
	?>
	<!--
	<div class="recentNews margintop-40">		
		<h4 class="headSide">Recent <span class="font-bold">News</span></h4>
		<div class="itemPopularNews">
			<ul>
				<li><a href="javascript:;">Disrinctively integrate real-time schmes</a></li>
				<li><a href="javascript:;">Credibly repurpose focused meta-service</a></li>
				<li><a href="javascript:;">Proactively benchmark functionalized deliverables</a></li>
				<li><a href="javascript:;">Credibly revolutionize diverse leadership</a></li>
				<li><a href="javascript:;">Conveniently build principle-centered</a></li>
			</ul>
		</div>		
	</div>
	-->
	<?php
		}
	?>

</div>