<footer>
	<div class="glanceFooter">		
		<div class="container">
			<div class="row">
				
				<div class="menuFooter wrItem">
					<div class="col-3">
						<ul>
							<li><a href="javascript:;" class="active">Home</a></li>
							<li><a href="javascript:;">About</a></li>
							<li><a href="javascript:;">Service</a></li>
							<li><a href="javascript:;">Products</a></li>						
						</ul>
						<div class="copyright">&copy; <?php echo date('Y');?> Panbil.co.id. All rights reserved.</div>
					</div>
					<div class="col-3">
						<div class="logoFooter">
							<a href="javascript:;">
								<img src="<?php echo base_url('images/public/logo-grey.png');?>" alt="Panbil" />
							</a>
						</div>
					</div>
					<div class="col-3">
						<ul>
							<li><a href="javascript:;">Partner</a></li>
							<li><a href="javascript:;">Gallery</a></li>
							<li><a href="javascript:;">News</a></li>
							<li><a href="javascript:;">Contact</a></li>
						</ul>
						<div class="tos">
							<div class="itemTos"><a href="javascript:;">Term of Service</a></div>
							<div class="itemTos"><a href="javascript:;">Site Map</a></div>
							<div class="itemTos"><a href="javascript:;">Help</a></div>
							<div class="itemTos"><a href="javascript:;" class="active">Career</a></div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</footer>



<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-76721918-1', 'auto');
	ga('send', 'pageview');

</script>
<?php if ($web['setting_google_analytics']) : ?>
<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', '<?=$web['setting_google_analytics']?>']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

</script>
<?php endif; ?>
</body>
</html>