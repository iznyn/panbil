<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Go Online Solusi Web Development | www.gositus.com | www.facebook.com/gositus | http://twitter.com/gositus -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<title><?php echo $title, ' | ' ,$web['setting_web_title'] ?></title>

<?php if ($web['setting_meta_desc']) echo '<meta name="description" content="', keywords($title,false) , $web['setting_meta_desc'] , '" />'; ?>
<?php if ($web['setting_meta_key']) echo '<meta name="keywords" content="', $web['setting_meta_key'] , '" />'; ?>

<!-- <link rel="shortcut icon" href="<?php echo base_url('images/favicon/16x16px.png');?>" />
<link rel="apple-touch-icon" href="<?php echo base_url('images/favicon/60x60px.png');?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('images/favicon/76x76px.png');?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('images/favicon/120x120px.png');?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('images/favicon/152x152px.png');?>"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(), 'css/style.css'; ?>" media="screen, projection, print" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(), 'css/style768.css'; ?>" media="screen and (min-width:0px) and (max-width:1191px)" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(), 'css/style480.css'; ?>" media="screen and (min-width:0px) and (max-width:850px)" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(), 'css/style320.css'; ?>" media="screen and (min-width:0px) and (max-width:479px)" />
<?php foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="', base_url(), 'css/', $style ,'.css" />'; ?>

<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
<script type="text/javascript" src="<?php echo base_url(), 'js/jquery-1.11.3.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(), 'js/jquery-ui.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(), 'js/tweenmax/Tweenmax.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(), 'js/SmoothScroll.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(), 'js/lib.jquery.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url(), 'js/global.js'; ?>"></script>
<?php foreach ($js as $script) echo '<script type="text/javascript" src="', base_url(), 'js/', $script ,'.js"></script>'; ?>


</head>

<body>

<div class="bgLoading">
	<div class="logoWait"></div>
	<div class="loadingBar"></div>
	<div class="percen"></div>	
</div>

<header>
	<div class="container">
		<div class="row">
			<div class="wrDef wrHeader">
				<div class="col-3">
					<ul class="menu">
						<li><a href="javascript:;" class="active">Home</a></li>
						<li><a href="javascript:;">About Us</a></li>
						<li><a href="javascript:;">Services</a></li>
						<li><a href="javascript:;">Products</a></li>
					</ul>	
				</div>
				<div class="col-3">
					<a href="javaascript:;" class="logo">
						<img src="<?php echo base_url('images/public/logo.png');?>" alt="Panbil" />
					</a>					
				</div>
				<div class="col-3">
					<ul class="menu">
						<li><a href="javascript:;">Partner</a></li>
						<li><a href="javascript:;">Gallery</a></li>
						<li><a href="javascript:;">News</a></li>
						<li><a href="javascript:;">Contact</a></li>
					</ul>
				</div>
			</div>			

			<div class="menu-mobile-toggle" style="display:none;">
				<a href="#">
					<img src="images/public/bars-black.png" alt="">
				</a>
			</div>
		</div>
	</div>
</header>

<div class="menu-mobile">
	<div class="menu-mobile-header">
		<img src="images/public/logo-white.png" alt="Panbil" />
	</div>
  <ul class="menu-mobile-nav">
  	<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;" class="active">About Us</a></li>
		<li><a href="javascript:;">Services</a></li>
		<li><a href="javascript:;">Products</a></li>
		<li><a href="javascript:;">Partner</a></li>
		<li><a href="javascript:;">Gallery</a></li>
		<li><a href="javascript:;">News</a></li>
		<li><a href="javascript:;">Contact</a></li>
  </ul>
  <a class="menu-mobile-close" href="">
  	<img src="images/public/icon-times.png" alt="">
  </a>
</div>

<div class="menuFix">
	<div class="container">
		<div class="row">
			<div class="wrDef wrHeader">
				<div class="col-3">
					<ul class="menu">
						<li><a href="javascript:;" class="active">Home</a></li>
						<li><a href="javascript:;">About Us</a></li>
						<li><a href="javascript:;">Services</a></li>
						<li><a href="javascript:;">Products</a></li>
					</ul>	
				</div>
				<div class="col-3">					
					<a href="javascript:;" class="logo">						
						<img src="<?php echo base_url('images/public/logo-white.png');?>" alt="Panbil" />
					</a>
				</div>
				<div class="col-3">
					<ul class="menu">
						<li><a href="javascript:;">Partner</a></li>
						<li><a href="javascript:;">Gallery</a></li>
						<li><a href="javascript:;">News</a></li>
						<li><a href="javascript:;">Contact</a></li>
					</ul>
				</div>
			</div>			

			<div class="menu-mobile-toggle" style="display:none;">
				<a href="#">
					<img src="images/public/bars.png" alt="">
				</a>
			</div>
		</div>
	</div>
</div>