<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/view/', $row[$first]['unique_id']; ?>" method="post" enctype="multipart/form-data">
    	<input type="hidden" name="id" id="item-id" value="<?php echo $row[$first]['unique_id']; ?>" />
        
        <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$first]['jobvacancy_name'])); ?>
        
        <div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    
                    <div class="language lang-<?=$lang['language_code']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <p>
                            <label for="jobvacancy_name_<?=$lang['language_id']?>">Job Title</label>
                            <input type="text" class="input-text required" name="jobvacancy_name_<?=$lang['language_id']?>" id="jobvacancy_name_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['jobvacancy_name']; ?>" />
                        </p>
						<p>
                            <label for="jobvacancy_bussiness_unit_<?=$lang['language_id']?>">Work Location</label>
                            <input type="text" class="input-text required" name="jobvacancy_bussiness_unit_<?=$lang['language_id']?>" id="jobvacancy_bussiness_unit_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['jobvacancy_bussiness_unit']; ?>" />
							<span class="help">Contoh : Puninar Head Office, Cakung - Jakarta</span>
                        </p>
						<p>
                            <label for="jobvacancy_working_time_<?=$lang['language_id']?>">Working Time</label>
                            <input type="text" class="input-text required" name="jobvacancy_working_time_<?=$lang['language_id']?>" id="jobvacancy_working_time_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['jobvacancy_working_time']; ?>" />
							<span class="help">Contoh : Full Time</span>
                        </p>
						<p>
							<label for="jobvacancy_description_<?=$lang['language_id']?>">Job Description</label>
							<textarea class="input-text required" name="jobvacancy_description_<?=$lang['language_id']?>" id="jobvacancy_description_<?=$lang['language_id']?>"><?php echo $row[$lang['language_id']]['jobvacancy_description']; ?></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
						<p>
							<label for="jobvacancy_requirements_<?=$lang['language_id']?>">Requirements</label>
							<textarea class="input-text required" name="jobvacancy_requirements_<?=$lang['language_id']?>" id="jobvacancy_requirements_<?=$lang['language_id']?>"><?php echo $row[$lang['language_id']]['jobvacancy_requirements']; ?></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
						<p>
							<label for="jobvacancy_benefits_<?=$lang['language_id']?>">Benefits</label>
							<textarea class="input-text required" name="jobvacancy_benefits_<?=$lang['language_id']?>" id="jobvacancy_benefits_<?=$lang['language_id']?>"><?php echo $row[$lang['language_id']]['jobvacancy_benefits']; ?></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
                    </div>
                    <p class="select">
                        <label for="jobvacancy_category">Category</label>
                        <select name="jobvacancy_category" class="input-text required" id="jobvacancy_category">
                            <option value="">-- Select Category --</option>
                            <?php foreach ($categories->result_array() as $item)
                            {
                                $select = ($item['jobcategory_id'] == $row[$first]['jobvacancy_category']) ? 'selected="selected"' : '';
                            echo '<option ' . $select . ' value="' . $item['jobcategory_id'] . '">' , $item['jobcategory_name'] , '</option>';
                            }?>
                        </select>
                        <span class="help"></span>
                    </p>
                    <?php $x++; endforeach; ?>
                    
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/view_flag'); ?> 
            </div>
            
            <div class="clear"></div>
        </div>
    </form>
</div>