<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/add'; ?>" method="post" enctype="multipart/form-data">
    
    	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
    
    	<div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    <div class="language lang-<?=$lang['language_code']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <p>
                            <label for="jobvacancy_name_<?=$lang['language_id']?>">Job Title</label>
                            <input type="text" class="input-text required" name="jobvacancy_name_<?=$lang['language_id']?>" id="jobvacancy_name_<?=$lang['language_id']?>" />
                        </p>
						<p>
                            <label for="jobvacancy_bussiness_unit_<?=$lang['language_id']?>">Work Location</label>
                            <input type="text" class="input-text required" name="jobvacancy_bussiness_unit_<?=$lang['language_id']?>" id="jobvacancy_bussiness_unit_<?=$lang['language_id']?>" />
							<span class="help">Contoh : Puninar Head Office, Cakung - Jakarta</span>
                        </p>
						<p>
                            <label for="jobvacancy_working_time_<?=$lang['language_id']?>">Working Time</label>
                            <input type="text" class="input-text required" name="jobvacancy_working_time_<?=$lang['language_id']?>" id="jobvacancy_working_time_<?=$lang['language_id']?>" />
							<span class="help">Contoh : Full Time</span>
                        </p>
						<p>
							<label for="jobvacancy_description_<?=$lang['language_id']?>">Job Description</label>
							<textarea class="input-text required" name="jobvacancy_description_<?=$lang['language_id']?>" id="jobvacancy_description_<?=$lang['language_id']?>"></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
						<p>
							<label for="jobvacancy_requirements_<?=$lang['language_id']?>">Requirements</label>
							<textarea class="input-text required" name="jobvacancy_requirements_<?=$lang['language_id']?>" id="jobvacancy_requirements_<?=$lang['language_id']?>"></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
						<p>
							<label for="jobvacancy_benefits_<?=$lang['language_id']?>">Benefits</label>
							<textarea class="input-text required" name="jobvacancy_benefits_<?=$lang['language_id']?>" id="jobvacancy_benefits_<?=$lang['language_id']?>"></textarea>
							<span class="help">Gunakan "~" (Tilde) sebagai pemisah baris</span>
						</p>
						<p class="select">
							<label for="article_section">Category</label>
							<select name="jobvacancy_category" class="input-text required" id="jobvacancy_category">
								<option value="">-- Select Category --</option>
								<?php foreach ($categories->result_array() as $item)
								echo '<option value="' . $item['jobcategory_id'] . '">' , $item['jobcategory_name'] , '</option>'; ?>
							</select>
							<span class="help"></span>
						</p>
                    </div>
                    <?php $x++; endforeach; ?>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/add_flag'); ?>
            </div>
            
            <div class="clear"></div>
         </div>
    </form>
</div>