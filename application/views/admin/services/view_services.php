<div id="content">
    	<form action="<?php echo base_url(), 'goadmin/', $url, '/view/', $row[$first]['unique_id']; ?>" method="post" <?php if ($this->services_image == TRUE) echo 'enctype="multipart/form-data"'; ?>>
    
        <input type="hidden" name="id" id="item-id" value="<?php echo $row[$first]['unique_id']; ?>" />
        
        <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$first]['services_name'])); ?>
        
        <div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php					
					$form = '';
					$x = 0;
					
					foreach (language()->result_array() as $lang) :
						
						$show = ($x == 0) ? 'style="display:block"' : '';
						$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
						$form .= '<p>';
						$form .= '<label for="services_name_' . $lang['language_id'] . '">Title</label>';
						
						$form .= '<input type="text" class="input-text required" name="services_name_' . $lang['language_id'] .'" id="services_name_' . $lang['language_id'] . '" value="' . $row[$lang['language_id']]['services_name'] . '" />';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="services_head_' . $lang['language_id'] . '">Heading</label>';
						$form .= '<input type="text" class="input-text required" name="services_head_' . $lang['language_id'] . '" id="services_head_' . $lang['language_id'] . '" value="' . $row[$lang['language_id']]['services_head'] . '" />';
						$form .= '<span class="help">Heading atau Intro Teks</span>';
						$form .= '</p>';
						$form .= '</div>';
						
						$x++;
					
					endforeach;
					
					echo $form;
					?>
                    
                    <p class="select">
                        <label for="services_section">Section</label>
                        <select name="services_section" class="input-text required" id="services_section">
                            <option value="">-- Select Section --</option>
                            <?php foreach ($sections->result_array() as $item)
                            {
                                $select = ($item['section_id'] == $row[$first]['services_section']) ? 'selected="selected"' : '';
                            echo '<option ' . $select . ' value="' . $item['section_id'] . '">' , $item['section_name'] , '</option>';
                            }?>
                        </select>
                        <span class="help">Pemilihan section / halaman di mana services akan muncul</span>
                    </p>
                    <?php if ($this->services_image == TRUE) : ?>
                    <p class="upload">
                        <?php if ($row[$lang['language_id']]['services_image']) : ?>
                        <img class="delete-image" title="Delete Image" alt="<?php echo $this->url; ?>" id="services_image" src="<?php echo base_url() , 'images/admin/delete.gif'; ?>" />
                        
                        <img class="load services_image" src="<?php echo base_url(), 'images/admin/ajax-loader.gif'; ?>" />
                        
                        <?php endif; ?>
                        <label>Image</label>
                        
                        <?php $image = explode("||",$row[$first]['services_image']); 
						if ($row[$first]['services_image']) echo '<a class="hover-image" href="', base_url() , 'images/services/' , $image[0] , '">&nbsp;</a>';
                        else echo '<span class="hover-image">&nbsp;</span>'; ?>
                        
                        <input type="text" class="input-text current-image" <?php if ($row[$first]['services_image']) echo 'value="hover to view current image"'; ?> />
                        
                        <input type="file" class="input-file" name="services_image" accept="jpg|jpeg|gif|png" size="36" />
                        
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help">Home -> Service = 105px * 105px</span>
                    </p>
					
					<!--
					<p class="upload">
                        <?php if ($row[$lang['language_id']]['services_image_content']) : ?>
                        <img class="delete-image" title="Delete Image" alt="<?php echo $this->url; ?>" id="services_image_content" src="<?php echo base_url() , 'images/admin/delete.gif'; ?>" />
                        
                        <img class="load services_image_content" src="<?php echo base_url(), 'images/admin/ajax-loader.gif'; ?>" />
                        
                        <?php endif; ?>
                        <label>Image Content</label>
                        
                        <?php if ($row[$first]['services_image_content']) echo '<a class="hover-image" href="', base_url() , 'images/services/' , $row[$first]['services_image_content'] , '">&nbsp;</a>';
                        else echo '<span class="hover-image">&nbsp;</span>'; ?>
                        
                        <input type="text" class="input-text current-image" <?php if ($row[$first]['services_image_content']) echo 'value="hover to view current image"'; ?> />
                        
                        <input type="file" class="input-file" name="services_image_content" accept="jpg|jpeg|gif|png" size="36" />
                        
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help">Other services = <?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
					-->
                    <?php endif; ?>
                    <p>
                        <label for="services_link">Link</label>
                        <input type="text" class="input-text url" name="services_link" id="services_link" value="<?php echo $row[$first]['services_link'];?>" />                        
                    </p>
                    <p>
                        <label for="sort">Sort</label>
                        <input type="text" class="input-text number" name="sort" id="sort" maxlength="2" value="<?php echo $row[$first]['sort'];?>"/>                        
                    </p>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/view_flag'); ?> 
            </div>
            
            <div class="clear"></div>
            
            <?php
			$form = '';
			$x = 0;
			foreach (language()->result_array() as $lang) :
			
				$show = ($x == 0) ? 'style="display:block"' : '';
				$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
				$form .= '<div class="form-div ckeditor">';
				$form .= '<h3>Content</h3>';
				$form .= '<textarea class="content" name="services_content_' . $lang['language_id'] . '">' . $row[$lang['language_id']]['services_content'] . '</textarea>';
				$form .= '</div>';
				$form .= '</div>';
				
				$x++;
		
			endforeach;
			
			echo $form;
			?>
        </div>
    </form>
</div>

<script type="text/javascript">

$(document).ready(function(){
     $('.banner_row').each(function(){
        
            var index = $(this).attr("data-index");

            //remove add category button manually
            if(index>1){
                $(this).find('.del_banner_btn').attr('style','display: inline-block !important');
                
            }
        }); 

        $('#add_banner').live('click',function(){
            var row = $('.banner_row');
            var last_id = $(".banner_row:last").attr('data-index');
            var next_id = parseInt(int(last_id)+1);

            var clone = row.eq(0).clone();
            clone.find("input:text").val("");

            if(clone.find(".delete-image") && clone.find(".hover-image")){
                clone.find("input[type=hidden]").remove();
                clone.find(".delete-image").remove();
                clone.find(".hover-image").remove();
            }

            clone.insertBefore(this).attr("data-index",next_id);

            if(next_id>=2){
                $('.del_banner_btn').attr('style','display: inline-block !important');
            }

                $('.banner_row:first').find('.del_banner_btn').attr('style','display: none !important');
             $("html, body").animate({ scrollTop: $(document).height()-$(window).height() },1000);
        });

        $(".del_banner_btn").live("click",function(){
            
            $target = $(this).closest(".banner_row");
            $target.hide('slow', function(){ $target.remove(); });
        });
        
});
</script>