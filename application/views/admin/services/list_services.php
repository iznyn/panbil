<div id="content">
	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'list')); ?>
        
    <table class="tablesorter" id="<?php echo $url; ?>">
    	<thead>
        	<tr>
            	<th width="35">No.</th>
            	<th>Name</th>
                <th>Section</th>
                <th>Sort</th>
                <th>Banner</th>
                <?php $this->load->view('admin/template/list_table_heading'); ?>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($query->result_array()) :
				$i = 1;
				foreach ($query->result_array() as $item) :
					$section = $this->db->select('section_name')->from('section')->where('section_id', $item['services_section'])->get()->row_array();
					
					if ($item['flag'] == 1) $color = '#090';
					elseif ($item['flag'] == 2) $color = '#F00'; ?>
					<tr>
						<td align="center"><?php echo $i; ?></td>
						<td><?php echo $item['services_name']; ?></td>
                        <td><?php echo $section['section_name']; ?></td>
						<td><?php echo ($item['sort'])? $item['sort'] : '-' ; ?></td>
						<td><?php 
							$datas = $this->db->get_where('banner_services', array('flag' => 1, 'banner_services_code' => $item['services_id']))->num_rows();
							$datas2 = $this->db->get_where('banner_services', array('flag' => 2, 'banner_services_code' => $item['services_id']))->num_rows();
							$datas3 = $this->db->get_where('banner_services', array('flag' => 3, 'banner_services_code' => $item['services_id']))->num_rows();
							echo "Active : <b><font color='green'>".$datas."</font></b>, Hide : <b><font color='red'>".$datas2."</font></b>, Deleted : <b>".$datas3."</b>"; 
						?></td>
						<td id="item-<?php echo $item['unique_id']; ?>" class="flag <?php echo $item['flag']; ?>"><?php if($item['services_section'] >= 9){?><span style="background:<?php echo $color; ?>;"></span><img class="load" src="<?php echo base_url(), 'images/admin/ajax-loader.gif' ?>" /><?php } ?></td>
						<td id="memo-<?php echo $item['unique_id']; ?>"><?php echo $item['flag_memo']; ?></td>
                        <td class="del">
                        	<a title="Edit &quot;<?php echo $item['services_name']; ?>&quot;" href="<?php echo base_url(), 'goadmin/', $url, '/view/', $item['unique_id']; ?>" class="input-submit edit">View</a>
                        	<?php if($item['services_section'] == 9){?><a title="Banner &quot;<?php echo $item['services_name']; ?>&quot;" href="<?php echo base_url(), 'goadmin', '/banner_services/bannerlist/', $item['services_id']; ?>" class="input-submit edit">Banner</a><?php } ?>
                        </td>
                        <td style="text-align: center;">
                            <?php if (check_access($this->url, 'delete'))
							{
								if($item['services_section'] == 9){
							?>                        
                            <input class="deletechecked" type="checkbox" value="<?php echo $item['unique_id']; ?>" />
                            <?php 
								}
							}
							?>
                        </td>  
					</tr>
				<?php
                $i++;
				endforeach;
			else :
				echo '<tr><td colspan="100%" align="center">No Data</td></td>';
			endif;
			?>
        </tbody>
        <tfoot>
        	<tr>
            	<th>No.</th>
            	<th>Name</th>
                <th>Section</th>
                <th>Sort</th>
				<th>Banner</th>
                <?php $this->load->view('admin/template/list_table_heading'); ?>
            </tr>
        </tfoot>
    </table>
</div>