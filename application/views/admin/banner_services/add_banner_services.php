<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/add'; ?>" method="post" enctype="multipart/form-data">
    
    	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
    
    	<div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    <div class="language lang-<?=$lang['language_code']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <p>
                            <label for="banner_services_name_<?=$lang['language_id']?>">Name</label>
                            <input type="hidden" class="input-text" name="banner_services_code" id="banner_services_code" value = "<?php echo $idservice; ?>" />
                            <input type="text" class="input-text required" name="banner_services_name_<?=$lang['language_id']?>" id="banner_services_name_<?=$lang['language_id']?>" />
                        </p>
                        <p>
                            <label for="banner_services_caption_<?=$lang['language_id']?>">Caption</label>
                            <input type="text" class="input-text" name="banner_services_caption_<?=$lang['language_id']?>" id="banner_services_caption_<?=$lang['language_id']?>" />
                        </p>
                    </div>
                    <?php $x++; endforeach; ?>
                    <!--
					<p class="select">
                        <label for="banner_services_code">Services Name</label>
                        <select name="banner_services_code" class="input-text required" id="banner_services_code">
                            <option value="">-- Select Services --</option>
                            <?php foreach ($services->result_array() as $item)
                            echo '<option value="' . $item['services_id'] . '">' , $item['services_name'] , '</option>'; ?>
                        </select>
                        <span class="help">Pemilihan services / halaman di mana banner akan muncul</span>
                    </p>
					-->
					<p>
                        <label for="banner_services_link">Link</label>
                        <input type="text" class="input-text url" name="banner_services_link" id="banner_services_link" />
                        <span class="help">Link yang akan dituju saat banner di-klik</span>
                    </p>
                    
                    <p class="upload">
                        <label for="banner_services_image">Image</label>
                        <input type="text" class="input-text" />
                        <input type="file" class="input-file" name="banner_services_image" accept="jpg|jpeg|gif|png" size="36" />
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution: <?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
                    
                    <p>
                        <label for="sort">Sort</label>
                        <input type="text" class="input-text number required" name="sort" id="sort" maxlength="2" />                        
                    </p>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/add_flag'); ?>
            </div>
            
            <div class="clear"></div>
         </div>
    </form>
</div>