<div id="content">
	<?php $this->load->view('admin/template/fixed_heading_services', array('type' => 'list')); ?>
        
    <table class="tablesorter" id="<?php echo $url; ?>">
    	<thead>
        	<tr>
            	<th width="35">No.</th>
            	<th>Service Name</th>
            	<th>Name</th>
            	<th>Banner Link</th>
                <th>Image Thumbnail</th>
                <th>Sort</th>
                <?php $this->load->view('admin/template/list_table_heading'); ?>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($query->result_array()) :
				$i = 1;
				foreach ($query->result_array() as $item) :
					if ($item['flag'] == 1) $color = '#090';
					elseif ($item['flag'] == 2) $color = '#F00'; ?>
					<tr>
						<td align="center"><?php echo $i; ?></td>
						<td><?php 
							$row = $this->db->get_where('services', array('services_id' => $item['banner_services_code']))->row();
							echo $row->services_name; 
						?></td>
						<td><?php echo $item['banner_services_name']; ?></td>
						<td><a href="<?php echo $item['banner_services_link']; ?>"><?php echo $item['banner_services_link']; ?></a></td>
                        <td><img width="100" src="<?php echo base_url(), 'images/services/', $item['banner_services_image']; ?>" /></td>
						<td><?php echo ($item['sort'])? $item['sort'] : '-';?></td>
                        <td id="item-<?php echo $item['unique_id']; ?>" class="flag <?php echo $item['flag']; ?>"><span style="background:<?php echo $color; ?>;"></span><img class="load" src="<?php echo base_url(), 'images/admin/ajax-loader.gif' ?>" /></td>
						<td id="memo-<?php echo $item['unique_id']; ?>"><?php echo $item['flag_memo']; ?></td>

                        <td class="del">
                        	<a title="Edit &quot;<?php echo $item['banner_services_name']; ?>&quot;" href="<?php echo base_url(), 'goadmin/', $url, '/view/', $item['unique_id']; ?>" class="input-submit edit">View</a>
                        </td>
                        <td style="text-align: center;">
                            <?php if (check_access($this->url, 'delete')) : ?>                        
                            <input class="deletechecked" type="checkbox" value="<?php echo $item['unique_id']; ?>" />
                            <?php endif; ?>
                        </td>  
					</tr>
				<?php
                $i++;
				endforeach;
			else :
				echo '<tr><td colspan="100%" align="center">No Data</td></td>';
			endif;
			?>
        </tbody>
        <tfoot>
        	<tr>
            	<th>No.</th>
            	<th>Service Name</th>
            	<th>Name</th>
            	<th>Banner Link</th>
                <th>Image Thumbnail</th>
                <th>Sort</th>
                <?php $this->load->view('admin/template/list_table_heading'); ?>
            </tr>
        </tfoot>
    </table>
</div>