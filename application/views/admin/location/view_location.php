<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/view/', $row[$first]['unique_id']; ?>" method="post" enctype="multipart/form-data">
    	<input type="hidden" name="id" id="item-id" value="<?php echo $row[$first]['unique_id']; ?>" />
        
        <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$first]['location_name'])); ?>
        
        <div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    
                    <div class="language lang-<?=$lang['language_code']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <p>
                            <label for="location_name_<?=$lang['language_id']?>">Location Name</label>
                            <input type="text" class="input-text required" name="location_name_<?=$lang['language_id']?>" id="location_name_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['location_name']; ?>" />
                        </p>
						<p>
                            <label for="location_gmap_<?=$lang['language_id']?>">Google Map</label>
                            <input type="text" class="input-text required" name="location_gmap_<?=$lang['language_id']?>" id="location_gmap_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['location_gmap']; ?>" />
                        </p>
                        <p>
                            <label for="location_address_<?=$lang['language_id']?>">Address</label>
                            <input type="text" class="input-text required" name="location_address_<?=$lang['language_id']?>" id="location_address_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['location_address']; ?>" />
                        </p>
                        <p>
                            <label for="location_sort_<?=$lang['language_id']?>">Sorting Number</label>
                            <input type="text" class="input-text required" name="location_sort_<?=$lang['language_id']?>" id="location_sort_<?=$lang['language_id']?>" value="<?php echo $row[$lang['language_id']]['location_sort']; ?>" />
                        </p>
                    </div>
                    
                    <?php $x++; endforeach; ?>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/view_flag'); ?> 
            </div>
            
            <div class="clear"></div>
        </div>
    </form>
</div>