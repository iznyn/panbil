<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/add'; ?>" method="post" enctype="multipart/form-data">
    
    	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
    
    	<div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php $x = 0; foreach (language()->result_array() as $lang) : ?>
                    <div class="language lang-<?=$lang['language_code']?>" <?php if ($x == 0) echo 'style="display:block"'; ?>>
                        <p>
                            <label for="location_name_<?=$lang['language_id']?>">Location</label>
                            <input type="text" class="input-text required" name="location_name_<?=$lang['language_id']?>" id="location_name_<?=$lang['language_id']?>" />
                        </p>
						<p>
                            <label for="location_gmap_<?=$lang['language_id']?>">Google Map</label>
                            <input type="text" class="input-text required" name="location_gmap_<?=$lang['language_id']?>" id="location_gmap_<?=$lang['language_id']?>" />
                        </p>
						<p>
                            <label for="location_address_<?=$lang['language_id']?>">Address</label>
                            <input type="text" class="input-text required" name="location_address_<?=$lang['language_id']?>" id="location_address_<?=$lang['language_id']?>" />
                        </p>
                        <p>
                            <label for="location_sort_<?=$lang['language_id']?>">Sorting Number</label>
                            <input type="text" class="input-text required" name="location_sort_<?=$lang['language_id']?>" id="location_sort_<?=$lang['language_id']?>" />
                        </p>
                    </div>
                    <?php $x++; endforeach; ?>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/add_flag'); ?>
            </div>
            
            <div class="clear"></div>
         </div>
    </form>
</div>