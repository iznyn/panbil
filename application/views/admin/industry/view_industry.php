<div id="content">
    	<form action="<?php echo base_url(), 'goadmin/', $url, '/view/', $row[$first]['unique_id']; ?>" method="post" <?php if ($this->industry_image == TRUE) echo 'enctype="multipart/form-data"'; ?>>
    
        <input type="hidden" name="id" id="item-id" value="<?php echo $row[$first]['unique_id']; ?>" />
        
        <?php $this->load->view('admin/template/fixed_heading', array('type' => 'view', 'name' => $row[$first]['industry_name'])); ?>
        
        <div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php					
					$form = '';
					$x = 0;
					
					foreach (language()->result_array() as $lang) :
						
						$show = ($x == 0) ? 'style="display:block"' : '';
						$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
						$form .= '<p>';
						$form .= '<label for="industry_name_' . $lang['language_id'] . '">Title</label>';
						
						$form .= '<input type="text" class="input-text required" name="industry_name_' . $lang['language_id'] .'" id="industry_name_' . $lang['language_id'] . '" value="' . $row[$lang['language_id']]['industry_name'] . '" />';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="industry_head_' . $lang['language_id'] . '">Heading</label>';
						$form .= '<input type="text" class="input-text required" name="industry_head_' . $lang['language_id'] . '" id="industry_head_' . $lang['language_id'] . '" value="' . $row[$lang['language_id']]['industry_head'] . '" />';
						$form .= '<span class="help">Heading atau Intro Teks</span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="industry_head2_' . $lang['language_id'] . '">Heading 2</label>';
						$form .= '<textarea class="input-text" name="industry_head2_' . $lang['language_id'] . '" id="industry_head2_' . $lang['language_id'] . '">' . $row[$lang['language_id']]['industry_head2'] . '</textarea>';
						$form .= '<span class="help">Heading Ke 2 (Gunakan "~" Tilde sebagai pemisah baris)</span>';
						$form .= '</p>';
						$form .= '</div>';
						
						$x++;
					
					endforeach;
					
					echo $form;
					?>
                    
                    <p class="select">
                        <label for="industry_section">Section</label>
                        <select name="industry_section" class="input-text required" id="industry_section">
                            <option value="">-- Select Section --</option>
                            <?php foreach ($sections->result_array() as $item)
                            {
                                $select = ($item['section_id'] == $row[$first]['industry_section']) ? 'selected="selected"' : '';
                            echo '<option ' . $select . ' value="' . $item['section_id'] . '">' , $item['section_name'] , '</option>';
                            }?>
                        </select>
                        <span class="help">Pemilihan section / halaman di mana industry akan muncul</span>
                    </p>
                    <?php if ($this->industry_image == TRUE) : ?>
                    <p class="upload">
                        <?php if ($row[$lang['language_id']]['industry_image']) : ?>
                        <img class="delete-image" title="Delete Image" alt="<?php echo $this->url; ?>" id="industry_image" src="<?php echo base_url() , 'images/admin/delete.gif'; ?>" />
                        
                        <img class="load industry_image" src="<?php echo base_url(), 'images/admin/ajax-loader.gif'; ?>" />
                        
                        <?php endif; ?>
                        <label>Image</label>
                        
                        <?php if ($row[$first]['industry_image']) echo '<a class="hover-image" href="', base_url() , 'images/industry/' , $row[$first]['industry_image'] , '">&nbsp;</a>';
                        else echo '<span class="hover-image">&nbsp;</span>'; ?>
                        
                        <input type="text" class="input-text current-image" <?php if ($row[$first]['industry_image']) echo 'value="hover to view current image"'; ?> />
                        
                        <input type="file" class="input-file" name="industry_image" accept="jpg|jpeg|gif|png" size="36" />
                        
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
					
					<p class="upload">
                        <?php if ($row[$lang['language_id']]['industry_image_hover']) : ?>
                        <img class="delete-image" title="Delete Image" alt="<?php echo $this->url; ?>" id="industry_image_hover" src="<?php echo base_url() , 'images/admin/delete.gif'; ?>" />
                        
                        <img class="load industry_image_hover" src="<?php echo base_url(), 'images/admin/ajax-loader.gif'; ?>" />
                        
                        <?php endif; ?>
                        <label>Image Hover</label>
                        
                        <?php if ($row[$first]['industry_image_hover']) echo '<a class="hover-image" href="', base_url() , 'images/industry/' , $row[$first]['industry_image_hover'] , '">&nbsp;</a>';
                        else echo '<span class="hover-image">&nbsp;</span>'; ?>
                        
                        <input type="text" class="input-text current-image" <?php if ($row[$first]['industry_image_hover']) echo 'value="hover to view current image"'; ?> />
                        
                        <input type="file" class="input-file" name="industry_image_hover" accept="jpg|jpeg|gif|png" size="36" />
                        
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
					
					<p class="upload">
                        <?php if ($row[$lang['language_id']]['industry_image_content']) : ?>
                        <img class="delete-image" title="Delete Image" alt="<?php echo $this->url; ?>" id="industry_image_content" src="<?php echo base_url() , 'images/admin/delete.gif'; ?>" />
                        
                        <img class="load industry_image_content" src="<?php echo base_url(), 'images/admin/ajax-loader.gif'; ?>" />
                        
                        <?php endif; ?>
                        <label>Image Content</label>
                        
                        <?php if ($row[$first]['industry_image_content']) echo '<a class="hover-image" href="', base_url() , 'images/industry/' , $row[$first]['industry_image_content'] , '">&nbsp;</a>';
                        else echo '<span class="hover-image">&nbsp;</span>'; ?>
                        
                        <input type="text" class="input-text current-image" <?php if ($row[$first]['industry_image_content']) echo 'value="hover to view current image"'; ?> />
                        
                        <input type="file" class="input-file" name="industry_image_content" accept="jpg|jpeg|gif|png" size="36" />
                        
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo "Unspecified"; ?></span>
                    </p>
                    <?php endif; ?>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/view_flag'); ?> 
            </div>
            
            <div class="clear"></div>
            
            <?php
			$form = '';
			$x = 0;
			foreach (language()->result_array() as $lang) :
			
				$show = ($x == 0) ? 'style="display:block"' : '';
				$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
				$form .= '<div class="form-div ckeditor">';
				$form .= '<h3>Content</h3>';
				$form .= '<textarea class="content" name="industry_content_' . $lang['language_id'] . '">' . $row[$lang['language_id']]['industry_content'] . '</textarea>';
				$form .= '</div>';
				$form .= '</div>';
				
				$x++;
		
			endforeach;
			
			echo $form;
			?>
        </div>
    </form>
</div>