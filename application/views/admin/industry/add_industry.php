<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/add'; ?>" method="post" <?php if ($this->industry_image == TRUE) echo 'enctype="multipart/form-data"'; ?>>
    
    	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
    
    	<div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php
					$form = '';
					$x = 0;
					foreach (language()->result_array() as $lang) :
					
						$show = ($x == 0) ? 'style="display:block"' : '';
						$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
						$form .= '<p>';
						$form .= '<label for="industry_name_' . $lang['language_id'] . '">Title</label>';
						$form .= '<input type="text" class="input-text required" name="industry_name_' . $lang['language_id'] .'" id="industry_name_' . $lang['language_id'] . '" />';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="industry_head_' . $lang['language_id'] . '">Heading</label>';
						$form .= '<input type="text" class="input-text required" name="industry_head_' . $lang['language_id'] . '" id="industry_head_' . $lang['language_id'] . '" />';
						$form .= '<span class="help">Heading atau Intro Teks</span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="industry_head2_' . $lang['language_id'] . '">Heading 2</label>';
						$form .= '<textarea class="input-text" name="industry_head2_' . $lang['language_id'] . '" id="industry_head2_' . $lang['language_id'] . '"></textarea>';
						$form .= '<span class="help">Heading Ke 2 (Gunakan "~" Tilde sebagai pemisah baris)</span>';
						$form .= '</p>';
						$form .= '</div>';
						
						$x++;
					
					endforeach;
					
					echo $form;
					?>
                    
                    <p class="select">
                        <label for="industry_section">Section</label>
                        <select name="industry_section" class="input-text required" id="industry_section">
                            <option value="">-- Select Section --</option>
                            <?php foreach ($sections->result_array() as $item)
                            echo '<option value="' . $item['section_id'] . '">' , $item['section_name'] , '</option>'; ?>
                        </select>
                        <span class="help">Pemilihan section / halaman di mana industry akan muncul</span>
                    </p>
                    
                    <?php if ($this->industry_image == TRUE) : ?>
                    <p class="upload">
                        <label for="industry_image">Image</label>
                        <input type="text" class="input-text" />
                        <input type="file" class="input-file" name="industry_image" accept="jpg|jpeg|gif|png" size="36" />
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
					<p class="upload">
                        <label for="industry_image_hover">Image Hover</label>
                        <input type="text" class="input-text" />
                        <input type="file" class="input-file" name="industry_image_hover" accept="jpg|jpeg|gif|png" size="36" />
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo $this->image_width, 'px * ', $this->image_height, 'px'; ?></span>
                    </p>
					<p class="upload">
                        <label for="industry_image_content">Image Content</label>
                        <input type="text" class="input-text" />
                        <input type="file" class="input-file" name="industry_image_content" accept="jpg|jpeg|gif|png" size="36" />
                        <input type="button" class="input-button" value="Browse" />
                        <span class="help">Recommended Resolution:</span>
                        <span class="help"><?php echo "Unspecified"; ?></span>
                    </p>
                    <?php endif;  ?>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/add_flag'); ?>
            </div>
            
            <div class="clear"></div>
            
            <?php
			$form = '';
			$x = 0;
			foreach (language()->result_array() as $lang) :
			
				$show = ($x == 0) ? 'style="display:block"' : '';
				$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
				$form .= '<div class="form-div ckeditor">';
				$form .= '<h3>Content</h3>';
				$form .= '<textarea class="content" name="industry_content_' . $lang['language_id'] . '"></textarea>';
				$form .= '</div>';
				$form .= '</div>';
				
				$x++;
		
			endforeach;
			
			echo $form;
			?>
         </div>
    </form>
</div>