<div id="content">
    <form action="<?php echo base_url(), 'goadmin/', $url, '/add'; ?>" method="post" enctype="multipart/form-data">
    
    	<?php $this->load->view('admin/template/fixed_heading', array('type' => 'add')); ?>
    
    	<div id="form-content">
            <div id="form-left">
                <div class="form-div">
                    <h3>Information</h3>
                    
                    <?php
					$form = '';
					$x = 0;
					foreach (language()->result_array() as $lang) :
					
						$show = ($x == 0) ? 'style="display:block"' : '';
						$form .= '<div class="language lang-' . $lang['language_code'] . '" ' . $show . '>';
						$form .= '<p>';
						$form .= '<label for="network_name_' . $lang['language_id'] . '">Network Name</label>';
						$form .= '<input type="text" class="input-text required" name="network_name_' . $lang['language_id'] .'" id="network_name_' . $lang['language_id'] . '" />';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_description_' . $lang['language_id'] . '">Description</label>';
						$form .= '<input type="text" class="input-text required" name="network_description_' . $lang['language_id'] . '" id="network_description_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_address_' . $lang['language_id'] . '">Address</label>';
						$form .= '<input type="text" class="input-text required" name="network_address_' . $lang['language_id'] . '" id="network_address_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_city_' . $lang['language_id'] . '">City</label>';
						$form .= '<input type="text" class="input-text required" name="network_city_' . $lang['language_id'] . '" id="network_city_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_postcode_' . $lang['language_id'] . '">Post Code</label>';
						$form .= '<input type="text" class="input-text required" name="network_postcode_' . $lang['language_id'] . '" id="network_postcode_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_phone_' . $lang['language_id'] . '">Phone</label>';
						$form .= '<input type="text" class="input-text required" name="network_phone_' . $lang['language_id'] . '" id="network_phone_' . $lang['language_id'] . '" />';
						$form .= '<span class="help">Gunakan "," (Koma) Jika Phone Lebih dari satu.</span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_fax_' . $lang['language_id'] . '">Fax</label>';
						$form .= '<input type="text" class="input-text required" name="network_fax_' . $lang['language_id'] . '" id="network_fax_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="network_gmap_' . $lang['language_id'] . '">Google Map</label>';
						$form .= '<input type="text" class="input-text required" name="network_gmap_' . $lang['language_id'] . '" id="network_gmap_' . $lang['language_id'] . '" />';
						$form .= '<span class="help"></span>';
						$form .= '</p>';
						
						$form .= '<p>';
						$form .= '<label for="sort_' . $lang['language_id'] . '">Sort Number</label>';
						$form .= '<input type="text" class="input-text required" name="sort_' . $lang['language_id'] .'" id="sort_' . $lang['language_id'] . '" />';
						$form .= '</p>';
						$form .= '</div>';
						
						$x++;
					
					endforeach;
					
					echo $form;
					?>
                    
                    <p class="select">
                        <label for="network_location">Location</label>
                        <select name="network_location" class="input-text required" id="network_location">
                            <option value="">-- Select Location --</option>
                            <?php foreach ($locations->result_array() as $item)
                            echo '<option value="' . $item['location_id'] . '">' , $item['location_name'] , '</option>'; ?>
                        </select>
                        <span class="help"></span>
                    </p>
                </div>
            </div>
            
            <div id="form-right">
                <?php $this->load->view('admin/template/add_flag'); ?>
            </div>
         </div>
    </form>
</div>