/*
Navicat MySQL Data Transfer

Source Server         : Tiger
Source Server Version : 50161
Source Host           : 192.168.1.100:3306
Source Database       : cp_panbil

Target Server Type    : MYSQL
Target Server Version : 50161
File Encoding         : 65001

Date: 2016-06-02 14:22:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_mobile` varchar(100) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `admin_privilege` tinyint(4) NOT NULL DEFAULT '1',
  `module_list` varchar(255) NOT NULL,
  `access` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '1', 'Administrator', 'admin', '', '', '9ffa1aebc0303c57da6ebd95e263507e', '1', '', '', '1', '');
INSERT INTO `admin` VALUES ('2', '2', 'Go Online Solusi', 'gositus', '', '', '1fa87f6003f8f260e668f9c0b08e1b87', '1', '', '', '1', '');
INSERT INTO `admin` VALUES ('3', '3', 'ando', 'ando', 'ando@gmail.com', '085781292278', '9ffa1aebc0303c57da6ebd95e263507e', '3', '2,3,4,6,7,9,10,11,12,14,15,16,18,19,20,22,23,24', '11,11,11,11,11,11,11,0,11,11,11,11,0,11,11,11,11,11', '1', '');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `article_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `article_head` text NOT NULL,
  `article_image` varchar(255) NOT NULL,
  `article_content` text NOT NULL,
  `article_section` int(11) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', '1', 'Mission', 'mission', '-', '', '<p>Panbil Industrial Estate is committed to providing world class manufacturing facilities, excellent infrastucture and fully integrated support services to the investors. We also aim to creat a vibrant and dynamic environment where people and companies can grow and thrive together.</p>\n', '3', '1', '');
INSERT INTO `article` VALUES ('2', '2', '1', 'Competitive Edge Landing Text', 'competitive_edge_landing_text', 'Competitive Edge', '', '<p>There are many advantages to choose Panbil as your manufacturing destination. All sites are planned, developed, well managed and specifically designed to meet industial needs. Full service land is available for costumized, self-built and standard factory buildings.</p>\n', '4', '1', '');
INSERT INTO `article` VALUES ('3', '3', '1', 'Residential Property', 'residential_property', '-', '', '<p>Savor the rich magic of Batam Island : Villa Panbil is indulge in a private world of luxury with characteristic features. Set against the backdrop of the beauty of Duriangkang lake view and pockets of quiet corners enclosed by serene nature forest and landscaping. Villa Panbil offer you both the unique nature experience and all the comports of a connected and cosmopolitan lifestyle.</p>\n', '7', '1', '');
INSERT INTO `article` VALUES ('4', '4', '1', 'Tenant landing Text', 'tenant_landing_text', '-', '', '<p>Panbil is the bussines partner for investors who seek to achieve maximum global competitiveness and investmen returns. Successfull operations and it&#39;s economic contributions has placed Panbil as one of the top industrial estate on Batam. Panbil offers a sustainable environment for bussiness from all over the world to grow and excel together.</p>\n', '10', '1', '');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_name` varchar(255) NOT NULL,
  `banner_caption` varchar(255) NOT NULL,
  `banner_link` text NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_content` text NOT NULL,
  `sort` int(4) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner
-- ----------------------------

-- ----------------------------
-- Table structure for banner_services
-- ----------------------------
DROP TABLE IF EXISTS `banner_services`;
CREATE TABLE `banner_services` (
  `banner_services_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_services_name` varchar(255) NOT NULL,
  `banner_services_caption` varchar(255) NOT NULL,
  `banner_services_link` text NOT NULL,
  `banner_services_image` varchar(255) NOT NULL,
  `banner_services_code` int(11) NOT NULL,
  `sort` int(4) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`banner_services_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner_services
-- ----------------------------

-- ----------------------------
-- Table structure for faq
-- ----------------------------
DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `faq_name` text NOT NULL,
  `seo_url` text NOT NULL,
  `faq_content` text NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of faq
-- ----------------------------

-- ----------------------------
-- Table structure for gallery_category
-- ----------------------------
DROP TABLE IF EXISTS `gallery_category`;
CREATE TABLE `gallery_category` (
  `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `gallery_category_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `gallery_category_type` tinyint(4) NOT NULL,
  `gallery_category_parent` int(11) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`gallery_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery_category
-- ----------------------------

-- ----------------------------
-- Table structure for industry
-- ----------------------------
DROP TABLE IF EXISTS `industry`;
CREATE TABLE `industry` (
  `industry_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `industry_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `industry_head` text NOT NULL,
  `industry_head2` text NOT NULL,
  `industry_image` varchar(255) NOT NULL,
  `industry_image_hover` varchar(255) NOT NULL,
  `industry_image_content` varchar(255) NOT NULL,
  `industry_content` text NOT NULL,
  `industry_section` int(11) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`industry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of industry
-- ----------------------------

-- ----------------------------
-- Table structure for item
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `item_category` int(11) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `item_content` text NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item
-- ----------------------------

-- ----------------------------
-- Table structure for jobcategory
-- ----------------------------
DROP TABLE IF EXISTS `jobcategory`;
CREATE TABLE `jobcategory` (
  `jobcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `jobcategory_name` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`jobcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jobcategory
-- ----------------------------

-- ----------------------------
-- Table structure for jobvacancy
-- ----------------------------
DROP TABLE IF EXISTS `jobvacancy`;
CREATE TABLE `jobvacancy` (
  `jobvacancy_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `jobvacancy_name` varchar(255) NOT NULL,
  `jobvacancy_bussiness_unit` varchar(255) NOT NULL,
  `jobvacancy_working_time` varchar(255) NOT NULL,
  `jobvacancy_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jobvacancy_description` text NOT NULL,
  `jobvacancy_requirements` text NOT NULL,
  `jobvacancy_benefits` text NOT NULL,
  `jobvacancy_category` int(11) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`jobvacancy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jobvacancy
-- ----------------------------

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_name` varchar(100) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `language_icon` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', '1', 'Indonesia', 'id', 'flag-indonesia-icon.png', '1', '');

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `location_gmap` varchar(255) NOT NULL,
  `location_address` varchar(255) NOT NULL,
  `location_sort` tinyint(4) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of location
-- ----------------------------

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_admin_id` int(11) NOT NULL,
  `log_action` varchar(100) NOT NULL,
  `log_db` varchar(100) NOT NULL,
  `log_value` int(11) NOT NULL,
  `log_name` varchar(255) NOT NULL,
  `log_desc` varchar(255) NOT NULL,
  `log_ip` varchar(20) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log
-- ----------------------------
INSERT INTO `log` VALUES ('1', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-18 09:25:49');
INSERT INTO `log` VALUES ('2', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '202.152.63.245', '2016-04-18 09:44:20');
INSERT INTO `log` VALUES ('3', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '202.152.63.245', '2016-04-18 09:45:39');
INSERT INTO `log` VALUES ('4', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-18 13:14:26');
INSERT INTO `log` VALUES ('5', '1', 'ADD', 'services', '8', 'testing', 'ADDED services ( testing ) ', '122.102.40.66', '2016-04-18 13:15:05');
INSERT INTO `log` VALUES ('6', '1', 'UPDATE', 'services', '8', 'testing', 'MODIFY services ( testing ) ', '122.102.40.66', '2016-04-18 13:15:38');
INSERT INTO `log` VALUES ('7', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-18 14:14:47');
INSERT INTO `log` VALUES ('8', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-18 16:32:05');
INSERT INTO `log` VALUES ('9', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '122.102.40.66', '2016-04-18 16:32:31');
INSERT INTO `log` VALUES ('10', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-18 16:34:57');
INSERT INTO `log` VALUES ('11', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-18 16:36:18');
INSERT INTO `log` VALUES ('12', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-18 16:37:03');
INSERT INTO `log` VALUES ('13', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-18 16:37:34');
INSERT INTO `log` VALUES ('14', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-18 16:37:49');
INSERT INTO `log` VALUES ('15', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-18 16:42:13');
INSERT INTO `log` VALUES ('16', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-18 16:42:31');
INSERT INTO `log` VALUES ('17', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-18 16:43:35');
INSERT INTO `log` VALUES ('18', '1', 'UPDATE', 'services', '1', 'Supply Chain Solutions', 'MODIFY services ( Supply Chain Solutions ) ', '192.168.1.64', '2016-04-18 16:43:55');
INSERT INTO `log` VALUES ('19', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-19 09:04:25');
INSERT INTO `log` VALUES ('20', '1', 'UPDATE', 'network', '18', 'Papua Office', 'ACTIVATE network ( Papua Office ) - Reason: test', '192.168.1.64', '2016-04-19 09:19:47');
INSERT INTO `log` VALUES ('21', '1', 'UPDATE', 'network', '18', 'Sumatera Barat', 'MODIFY network ( Sumatera Barat ) ', '192.168.1.64', '2016-04-19 09:20:24');
INSERT INTO `log` VALUES ('22', '1', 'UPDATE', 'network', '16', 'Jambi Office', 'MODIFY network ( Jambi Office ) ', '192.168.1.64', '2016-04-19 09:23:13');
INSERT INTO `log` VALUES ('23', '1', 'UPDATE', 'network', '15', 'Medan Office', 'MODIFY network ( Medan Office ) ', '192.168.1.64', '2016-04-19 09:23:21');
INSERT INTO `log` VALUES ('24', '1', 'UPDATE', 'network', '18', 'Sumatera Barat', 'DEACTIVATE network ( Sumatera Barat ) - Reason: Hide', '192.168.1.64', '2016-04-19 09:23:48');
INSERT INTO `log` VALUES ('25', '1', 'UPDATE', 'network', '18', 'Sumatera Barat', 'ACTIVATE network ( Sumatera Barat ) - Reason: asdf', '192.168.1.64', '2016-04-19 09:28:32');
INSERT INTO `log` VALUES ('26', '1', 'UPDATE', 'network', '18', 'Sumatera Barat', 'DEACTIVATE network ( Sumatera Barat ) - Reason: hide', '192.168.1.64', '2016-04-19 09:47:06');
INSERT INTO `log` VALUES ('27', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-19 10:04:36');
INSERT INTO `log` VALUES ('28', '1', 'UPDATE', 'network', '17', 'Cakung Office', 'MODIFY network ( Cakung Office ) ', '122.102.40.66', '2016-04-19 10:05:29');
INSERT INTO `log` VALUES ('29', '1', 'UPDATE', 'network', '17', 'Cakung Office', 'MODIFY network ( Cakung Office ) ', '122.102.40.66', '2016-04-19 10:09:28');
INSERT INTO `log` VALUES ('30', '1', 'UPDATE', 'network', '1', 'Cakung Office', 'DEACTIVATE network ( Cakung Office ) - Reason: hide', '122.102.40.66', '2016-04-19 10:12:27');
INSERT INTO `log` VALUES ('31', '1', 'UPDATE', 'location', '1', 'Jakarta', 'MODIFY location ( Jakarta ) ', '122.102.40.66', '2016-04-19 10:16:06');
INSERT INTO `log` VALUES ('32', '1', 'UPDATE', 'location', '3', 'Sulawesi Utara', 'MODIFY location ( Sulawesi Utara ) ', '122.102.40.66', '2016-04-19 10:16:24');
INSERT INTO `log` VALUES ('33', '1', 'UPDATE', 'network', '17', 'Cakung Office', 'MODIFY network ( Cakung Office ) ', '122.102.40.66', '2016-04-19 10:18:01');
INSERT INTO `log` VALUES ('34', '1', 'UPDATE', 'network', '2', 'Nagrak Office', 'MODIFY network ( Nagrak Office ) ', '122.102.40.66', '2016-04-19 10:18:44');
INSERT INTO `log` VALUES ('35', '1', 'UPDATE', 'network', '8', 'Alam Sutera Office', 'MODIFY network ( Alam Sutera Office ) ', '122.102.40.66', '2016-04-19 10:19:18');
INSERT INTO `log` VALUES ('36', '1', 'UPDATE', 'network', '9', 'Soekarno-Hatta  Office', 'MODIFY network ( Soekarno-Hatta  Office ) ', '122.102.40.66', '2016-04-19 10:19:25');
INSERT INTO `log` VALUES ('37', '1', 'UPDATE', 'network', '9', 'Soekarno-Hatta  Office', 'MODIFY network ( Soekarno-Hatta  Office ) ', '122.102.40.66', '2016-04-19 10:19:53');
INSERT INTO `log` VALUES ('38', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-19 10:27:30');
INSERT INTO `log` VALUES ('39', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-19 10:51:09');
INSERT INTO `log` VALUES ('40', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-19 11:03:09');
INSERT INTO `log` VALUES ('41', '1', 'UPDATE', 'location', '1', 'Jakarta', 'MODIFY location ( Jakarta ) ', '122.102.40.66', '2016-04-19 11:44:32');
INSERT INTO `log` VALUES ('42', '1', 'UPDATE', 'location', '6', 'Jawa', 'MODIFY location ( Jawa ) ', '122.102.40.66', '2016-04-19 11:44:50');
INSERT INTO `log` VALUES ('43', '1', 'UPDATE', 'network', '17', 'Cakung Office', 'MODIFY network ( Cakung Office ) ', '122.102.40.66', '2016-04-19 11:45:43');
INSERT INTO `log` VALUES ('44', '1', 'UPDATE', 'network', '9', 'Soekarno-Hatta  Office', 'MODIFY network ( Soekarno-Hatta  Office ) ', '122.102.40.66', '2016-04-19 11:45:53');
INSERT INTO `log` VALUES ('45', '1', 'UPDATE', 'network', '9', 'Soekarno-Hatta  Office', 'MODIFY network ( Soekarno-Hatta  Office ) ', '122.102.40.66', '2016-04-19 11:46:33');
INSERT INTO `log` VALUES ('46', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-19 13:21:42');
INSERT INTO `log` VALUES ('47', '1', 'ADD', 'services', '9', 'Contoh Service Ke-5', 'ADDED services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 13:23:28');
INSERT INTO `log` VALUES ('48', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 13:33:54');
INSERT INTO `log` VALUES ('49', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 13:38:45');
INSERT INTO `log` VALUES ('50', '1', 'UPDATE', 'industry', '7', 'Oil, Gas & Mining', 'DEACTIVATE industry ( Oil, Gas & Mining ) - Reason: hide', '122.102.40.66', '2016-04-19 13:40:21');
INSERT INTO `log` VALUES ('51', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 13:40:45');
INSERT INTO `log` VALUES ('52', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 13:46:07');
INSERT INTO `log` VALUES ('53', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-19 13:46:43');
INSERT INTO `log` VALUES ('54', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-19 13:47:00');
INSERT INTO `log` VALUES ('55', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-19 13:47:11');
INSERT INTO `log` VALUES ('56', '1', 'UPDATE', 'services', '1', 'Supply Chain Solutions', 'MODIFY services ( Supply Chain Solutions ) ', '192.168.1.64', '2016-04-19 13:47:47');
INSERT INTO `log` VALUES ('57', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-19 13:54:02');
INSERT INTO `log` VALUES ('58', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-19 13:54:14');
INSERT INTO `log` VALUES ('59', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-19 13:54:25');
INSERT INTO `log` VALUES ('60', '1', 'UPDATE', 'services', '1', 'Supply Chain Solutions', 'MODIFY services ( Supply Chain Solutions ) ', '192.168.1.64', '2016-04-19 13:54:38');
INSERT INTO `log` VALUES ('61', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:00:44');
INSERT INTO `log` VALUES ('62', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:06:57');
INSERT INTO `log` VALUES ('63', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:07:23');
INSERT INTO `log` VALUES ('64', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:09:06');
INSERT INTO `log` VALUES ('65', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:12:05');
INSERT INTO `log` VALUES ('66', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:13:33');
INSERT INTO `log` VALUES ('67', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:14:09');
INSERT INTO `log` VALUES ('68', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-19 14:20:46');
INSERT INTO `log` VALUES ('69', '1', 'UPDATE', 'services', '3', 'Warehouse Management', 'MODIFY services ( Warehouse Management ) ', '192.168.1.64', '2016-04-19 14:20:58');
INSERT INTO `log` VALUES ('70', '1', 'UPDATE', 'services', '2', 'Multimodal Transportation', 'MODIFY services ( Multimodal Transportation ) ', '192.168.1.64', '2016-04-19 14:21:10');
INSERT INTO `log` VALUES ('71', '1', 'UPDATE', 'services', '1', 'Supply Chain Solutions', 'MODIFY services ( Supply Chain Solutions ) ', '192.168.1.64', '2016-04-19 14:21:26');
INSERT INTO `log` VALUES ('72', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-19 14:22:55');
INSERT INTO `log` VALUES ('73', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'DEACTIVATE services ( Contoh Service Ke-5 ) - Reason: Non aktif', '192.168.1.64', '2016-04-19 14:26:31');
INSERT INTO `log` VALUES ('74', '1', 'ADD', 'services', '10', 'Contoh Service Ke-6', 'ADDED services ( Contoh Service Ke-6 ) ', '192.168.1.64', '2016-04-19 17:05:07');
INSERT INTO `log` VALUES ('75', '1', 'UPDATE', 'services', '10', 'Contoh Service Ke-6', 'DEACTIVATE services ( Contoh Service Ke-6 ) - Reason: On Process', '192.168.1.64', '2016-04-19 17:55:35');
INSERT INTO `log` VALUES ('76', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-20 08:55:56');
INSERT INTO `log` VALUES ('77', '1', 'ADD', 'module', '33', 'Services Banner', 'ADDED module ( Services Banner ) ', '192.168.1.64', '2016-04-20 09:41:16');
INSERT INTO `log` VALUES ('78', '1', 'ADD', 'banner_services', '1', 'Contoh SCS 1', 'ADDED banner_services ( Contoh SCS 1 ) ', '192.168.1.64', '2016-04-20 10:02:11');
INSERT INTO `log` VALUES ('79', '1', 'UPDATE', 'banner_services', '1', 'Contoh SCS 1', 'DEACTIVATE banner_services ( Contoh SCS 1 ) - Reason: Hapus', '192.168.1.64', '2016-04-20 10:38:09');
INSERT INTO `log` VALUES ('80', '1', 'ADD', 'banner_services', '2', 'Contoh CC 1', 'ADDED banner_services ( Contoh CC 1 ) ', '192.168.1.64', '2016-04-20 10:39:54');
INSERT INTO `log` VALUES ('81', '1', 'UPDATE', 'banner_services', '2', 'Contoh CC 1', 'MODIFY banner_services ( Contoh CC 1 ) ', '192.168.1.64', '2016-04-20 10:40:20');
INSERT INTO `log` VALUES ('82', '1', 'UPDATE', 'banner_services', '2', 'Contoh CC 1', 'MODIFY banner_services ( Contoh CC 1 ) ', '192.168.1.64', '2016-04-20 10:40:57');
INSERT INTO `log` VALUES ('83', '1', 'ADD', 'services', '11', 'service 5', 'ADDED services ( service 5 ) ', '122.102.40.66', '2016-04-20 10:49:20');
INSERT INTO `log` VALUES ('84', '1', 'UPDATE', 'services', '11', 'service serviceservice service 5', 'MODIFY services ( service serviceservice service 5 ) ', '122.102.40.66', '2016-04-20 10:51:04');
INSERT INTO `log` VALUES ('85', '1', 'UPDATE', 'services', '11', 'service 5', 'MODIFY services ( service 5 ) ', '122.102.40.66', '2016-04-20 10:51:36');
INSERT INTO `log` VALUES ('86', '1', 'UPDATE', 'services', '11', 'service 5', 'MODIFY services ( service 5 ) ', '122.102.40.66', '2016-04-20 10:51:47');
INSERT INTO `log` VALUES ('87', '1', 'UPDATE', 'services', '11', 'service serviceservice5', 'MODIFY services ( service serviceservice5 ) ', '122.102.40.66', '2016-04-20 10:51:54');
INSERT INTO `log` VALUES ('88', '1', 'UPDATE', 'services', '11', 'service serviceservice5', 'MODIFY services ( service serviceservice5 ) ', '122.102.40.66', '2016-04-20 10:52:39');
INSERT INTO `log` VALUES ('89', '1', 'UPDATE', 'services', '11', 'service5', 'MODIFY services ( service5 ) ', '122.102.40.66', '2016-04-20 10:53:04');
INSERT INTO `log` VALUES ('90', '1', 'UPDATE', 'services', '11', 'service5', 'MODIFY services ( service5 ) ', '122.102.40.66', '2016-04-20 10:53:50');
INSERT INTO `log` VALUES ('91', '1', 'UPDATE', 'services', '11', 'service5', 'MODIFY services ( service5 ) ', '122.102.40.66', '2016-04-20 10:53:54');
INSERT INTO `log` VALUES ('92', '1', 'UPDATE', 'industry', '7', 'Oil, Gas & Mining', 'ACTIVATE industry ( Oil, Gas & Mining ) - Reason: ok', '122.102.40.66', '2016-04-20 10:55:41');
INSERT INTO `log` VALUES ('93', '1', 'UPDATE', 'industry', '2', 'Electronic & Technology', 'DEACTIVATE industry ( Electronic & Technology ) - Reason: test hide', '122.102.40.66', '2016-04-20 10:55:58');
INSERT INTO `log` VALUES ('94', '1', 'UPDATE', 'industry', '2', 'Electronic & Technology', 'ACTIVATE industry ( Electronic & Technology ) - Reason: ok', '122.102.40.66', '2016-04-20 10:57:06');
INSERT INTO `log` VALUES ('95', '1', 'UPDATE', 'industry', '3', 'Fashion & Lifestyle', 'MODIFY industry ( Fashion & Lifestyle ) ', '122.102.40.66', '2016-04-20 10:58:13');
INSERT INTO `log` VALUES ('96', '1', 'UPDATE', 'industry', '3', 'Fashion & Lifestyle', 'MODIFY industry ( Fashion & Lifestyle ) ', '122.102.40.66', '2016-04-20 11:00:12');
INSERT INTO `log` VALUES ('97', '1', 'ADD', 'banner_services', '3', 'Contoh CC 2', 'ADDED banner_services ( Contoh CC 2 ) ', '192.168.1.64', '2016-04-20 11:25:12');
INSERT INTO `log` VALUES ('98', '1', 'UPDATE', 'banner_services', '2', 'Contoh CC 1', 'MODIFY banner_services ( Contoh CC 1 ) ', '192.168.1.64', '2016-04-20 11:25:24');
INSERT INTO `log` VALUES ('99', '1', 'UPDATE', 'banner_services', '1', 'Contoh SCS 1', 'MODIFY banner_services ( Contoh SCS 1 ) ', '192.168.1.64', '2016-04-20 11:26:00');
INSERT INTO `log` VALUES ('100', '1', 'UPDATE', 'banner_services', '1', 'Contoh SCS 1', 'ACTIVATE banner_services ( Contoh SCS 1 ) - Reason: coba', '192.168.1.64', '2016-04-20 11:26:07');
INSERT INTO `log` VALUES ('101', '1', 'UPDATE', 'module', '33', 'Services Banner', 'DEACTIVATE module ( Services Banner ) - Reason: hide', '192.168.1.64', '2016-04-20 11:49:25');
INSERT INTO `log` VALUES ('102', '1', 'UPDATE', 'services', '11', 'service5', 'MODIFY services ( service5 ) ', '122.102.40.66', '2016-04-20 11:52:05');
INSERT INTO `log` VALUES ('103', '1', 'UPDATE', 'services', '11', 'service', 'MODIFY services ( service ) ', '122.102.40.66', '2016-04-20 11:52:14');
INSERT INTO `log` VALUES ('104', '1', 'UPDATE', 'services', '4', 'Custom Clearancee', 'MODIFY services ( Custom Clearancee ) ', '122.102.40.66', '2016-04-20 11:52:34');
INSERT INTO `log` VALUES ('105', '1', 'UPDATE', 'services', '11', 'service', 'MODIFY services ( service ) ', '122.102.40.66', '2016-04-20 11:53:44');
INSERT INTO `log` VALUES ('106', '1', 'UPDATE', 'services', '11', 'service', 'MODIFY services ( service ) ', '122.102.40.66', '2016-04-20 11:53:50');
INSERT INTO `log` VALUES ('107', '1', 'UPDATE', 'banner_services', '3', 'Contoh CC 2', 'DEACTIVATE banner_services ( Contoh CC 2 ) - Reason: hide', '122.102.40.66', '2016-04-20 11:56:05');
INSERT INTO `log` VALUES ('108', '1', 'UPDATE', 'banner_services', '2', 'Contoh CC 1', 'DEACTIVATE banner_services ( Contoh CC 1 ) - Reason: hide', '122.102.40.66', '2016-04-20 11:56:10');
INSERT INTO `log` VALUES ('109', '1', 'ADD', 'banner_services', '4', 'testing', 'ADDED banner_services ( testing ) ', '122.102.40.66', '2016-04-20 11:57:37');
INSERT INTO `log` VALUES ('110', '1', 'UPDATE', 'services', '4', 'Custom Clearancee', 'MODIFY services ( Custom Clearancee ) ', '192.168.1.64', '2016-04-20 11:59:28');
INSERT INTO `log` VALUES ('111', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-20 11:59:59');
INSERT INTO `log` VALUES ('112', '1', 'UPDATE', 'services', '4', 'Custom Clearance', 'MODIFY services ( Custom Clearance ) ', '192.168.1.64', '2016-04-20 12:00:18');
INSERT INTO `log` VALUES ('113', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'ACTIVATE services ( Contoh Service Ke-5 ) - Reason: test', '122.102.40.66', '2016-04-20 12:04:33');
INSERT INTO `log` VALUES ('114', '1', 'UPDATE', 'services', '11', 'service', 'DEACTIVATE services ( service ) - Reason: hide', '122.102.40.66', '2016-04-20 12:05:07');
INSERT INTO `log` VALUES ('115', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-52', 'MODIFY services ( Contoh Service Ke-52 ) ', '192.168.1.64', '2016-04-20 12:29:55');
INSERT INTO `log` VALUES ('116', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'MODIFY services ( Contoh Service Ke-5 ) ', '192.168.1.64', '2016-04-20 12:30:17');
INSERT INTO `log` VALUES ('117', '1', 'ADD', 'banner_services', '5', 'Service 5 - 1', 'ADDED banner_services ( Service 5 - 1 ) ', '192.168.1.64', '2016-04-20 12:40:56');
INSERT INTO `log` VALUES ('118', '1', 'ADD', 'banner_services', '6', 'Service 5 - 2', 'ADDED banner_services ( Service 5 - 2 ) ', '192.168.1.64', '2016-04-20 12:42:25');
INSERT INTO `log` VALUES ('119', '1', 'ADD', 'banner_services', '7', 'Service 5 - 3', 'ADDED banner_services ( Service 5 - 3 ) ', '192.168.1.64', '2016-04-20 12:43:07');
INSERT INTO `log` VALUES ('120', '1', 'ADD', 'banner_services', '8', 'Service 5 - 4', 'ADDED banner_services ( Service 5 - 4 ) ', '192.168.1.64', '2016-04-20 12:44:40');
INSERT INTO `log` VALUES ('121', '1', 'ADD', 'banner_services', '9', 'Service 5 - 5', 'ADDED banner_services ( Service 5 - 5 ) ', '192.168.1.64', '2016-04-20 12:45:38');
INSERT INTO `log` VALUES ('122', '1', 'UPDATE', 'banner_services', '9', 'Service 5 - 7', 'MODIFY banner_services ( Service 5 - 7 ) ', '192.168.1.64', '2016-04-20 12:47:07');
INSERT INTO `log` VALUES ('123', '1', 'UPDATE', 'services', '11', 'service', 'MODIFY services ( service ) ', '122.102.40.66', '2016-04-20 12:59:45');
INSERT INTO `log` VALUES ('124', '1', 'UPDATE', 'services', '11', 'servicee', 'MODIFY services ( servicee ) ', '122.102.40.66', '2016-04-20 13:00:09');
INSERT INTO `log` VALUES ('125', '1', 'UPDATE', 'services', '11', 'servicee', 'ACTIVATE services ( servicee ) - Reason: ok', '122.102.40.66', '2016-04-20 13:10:12');
INSERT INTO `log` VALUES ('126', '1', 'UPDATE', 'industry', '1', 'Automotive', 'MODIFY industry ( Automotive ) ', '192.168.1.64', '2016-04-20 13:10:49');
INSERT INTO `log` VALUES ('127', '1', 'ADD', 'banner_services', '10', 'testing', 'ADDED banner_services ( testing ) ', '122.102.40.66', '2016-04-20 13:12:20');
INSERT INTO `log` VALUES ('128', '1', 'ADD', 'banner_services', '11', 'testing', 'ADDED banner_services ( testing ) ', '122.102.40.66', '2016-04-20 13:12:33');
INSERT INTO `log` VALUES ('129', '1', 'UPDATE', 'banner_services', '8', 'Service 5 - 4', 'DEACTIVATE banner_services ( Service 5 - 4 ) - Reason: Coba', '192.168.1.64', '2016-04-20 13:31:52');
INSERT INTO `log` VALUES ('130', '1', 'UPDATE', 'banner_services', '8', 'Service 5 - 4', 'ACTIVATE banner_services ( Service 5 - 4 ) - Reason: aktif', '192.168.1.64', '2016-04-20 14:38:28');
INSERT INTO `log` VALUES ('131', '1', 'ADD', 'banner_services', '12', 'Service 5 - 8', 'ADDED banner_services ( Service 5 - 8 ) ', '192.168.1.64', '2016-04-20 14:38:48');
INSERT INTO `log` VALUES ('132', '1', 'ADD', 'banner_services', '13', 'Service 5 - 9', 'ADDED banner_services ( Service 5 - 9 ) ', '192.168.1.64', '2016-04-20 14:39:23');
INSERT INTO `log` VALUES ('133', '1', 'UPDATE', 'services', '11', 'servicee', 'DEACTIVATE services ( servicee ) - Reason: Hide', '192.168.1.64', '2016-04-20 14:46:32');
INSERT INTO `log` VALUES ('134', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'DEACTIVATE services ( Contoh Service Ke-5 ) - Reason: Hide', '192.168.1.64', '2016-04-20 14:46:36');
INSERT INTO `log` VALUES ('135', '1', 'UPDATE', 'services', '9', 'Contoh Service Ke-5', 'ACTIVATE services ( Contoh Service Ke-5 ) - Reason: contoh', '192.168.1.64', '2016-04-20 15:37:11');
INSERT INTO `log` VALUES ('136', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '115.178.212.198', '2016-04-20 22:07:20');
INSERT INTO `log` VALUES ('137', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '202.152.63.245', '2016-04-21 08:53:30');
INSERT INTO `log` VALUES ('138', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '122.102.40.66', '2016-04-21 09:38:43');
INSERT INTO `log` VALUES ('139', '1', 'UPDATE', 'services', '1', 'Supply Chain Solutions', 'MODIFY services ( Supply Chain Solutions ) ', '122.102.40.66', '2016-04-21 09:39:06');
INSERT INTO `log` VALUES ('140', '1', 'ADD', 'banner_services', '14', 'testing', 'ADDED banner_services ( testing ) ', '122.102.40.66', '2016-04-21 09:43:04');
INSERT INTO `log` VALUES ('141', '1', 'UPDATE', 'industry', '7', 'Oil, Gas & Mining', 'MODIFY industry ( Oil, Gas & Mining ) ', '122.102.40.66', '2016-04-21 09:47:56');
INSERT INTO `log` VALUES ('142', '1', 'UPDATE', 'industry', '7', 'Oil, Gas & Mining', 'MODIFY industry ( Oil, Gas & Mining ) ', '122.102.40.66', '2016-04-21 09:48:21');
INSERT INTO `log` VALUES ('143', '1', 'ADD', 'banner_services', '15', 'testing', 'ADDED banner_services ( testing ) ', '122.102.40.66', '2016-04-21 10:19:36');
INSERT INTO `log` VALUES ('144', '1', 'ADD', 'banner_services', '16', 'test1', 'ADDED banner_services ( test1 ) ', '122.102.40.66', '2016-04-21 10:24:37');
INSERT INTO `log` VALUES ('145', '1', 'ADD', 'banner_services', '17', 'wh banner1', 'ADDED banner_services ( wh banner1 ) ', '122.102.40.66', '2016-04-21 10:29:00');
INSERT INTO `log` VALUES ('146', '1', 'ADD', 'banner_services', '18', 'wh banner2', 'ADDED banner_services ( wh banner2 ) ', '122.102.40.66', '2016-04-21 10:29:54');
INSERT INTO `log` VALUES ('147', '1', 'ADD', 'banner_services', '19', 'wh banner3', 'ADDED banner_services ( wh banner3 ) ', '122.102.40.66', '2016-04-21 10:30:19');
INSERT INTO `log` VALUES ('148', '1', 'ADD', 'banner_services', '20', 'wh banner4', 'ADDED banner_services ( wh banner4 ) ', '122.102.40.66', '2016-04-21 10:31:45');
INSERT INTO `log` VALUES ('149', '1', 'ADD', 'banner_services', '21', 'wh banner5', 'ADDED banner_services ( wh banner5 ) ', '122.102.40.66', '2016-04-21 10:32:04');
INSERT INTO `log` VALUES ('150', '1', 'ADD', 'banner_services', '22', 'wh latar', 'ADDED banner_services ( wh latar ) ', '122.102.40.66', '2016-04-21 10:33:17');
INSERT INTO `log` VALUES ('151', '1', 'UPDATE', 'banner_services', '22', 'wh latar', 'MODIFY banner_services ( wh latar ) ', '122.102.40.66', '2016-04-21 10:33:45');
INSERT INTO `log` VALUES ('152', '1', 'UPDATE', 'banner_services', '17', 'wh banner1', 'MODIFY banner_services ( wh banner1 ) ', '122.102.40.66', '2016-04-21 10:33:53');
INSERT INTO `log` VALUES ('153', '1', 'UPDATE', 'banner_services', '18', 'wh banner2', 'MODIFY banner_services ( wh banner2 ) ', '122.102.40.66', '2016-04-21 10:33:59');
INSERT INTO `log` VALUES ('154', '1', 'UPDATE', 'banner_services', '19', 'wh banner3', 'MODIFY banner_services ( wh banner3 ) ', '122.102.40.66', '2016-04-21 10:34:05');
INSERT INTO `log` VALUES ('155', '1', 'UPDATE', 'banner_services', '20', 'wh banner4', 'MODIFY banner_services ( wh banner4 ) ', '122.102.40.66', '2016-04-21 10:34:11');
INSERT INTO `log` VALUES ('156', '1', 'UPDATE', 'banner_services', '21', 'wh banner5', 'MODIFY banner_services ( wh banner5 ) ', '122.102.40.66', '2016-04-21 10:34:16');
INSERT INTO `log` VALUES ('157', '1', 'UPDATE', 'banner_services', '17', 'wh banner1', 'MODIFY banner_services ( wh banner1 ) ', '122.102.40.66', '2016-04-21 10:47:55');
INSERT INTO `log` VALUES ('158', '1', 'UPDATE', 'banner_services', '22', 'wh latar', 'MODIFY banner_services ( wh latar ) ', '122.102.40.66', '2016-04-21 10:48:00');
INSERT INTO `log` VALUES ('159', '1', 'UPDATE', 'banner_services', '22', 'wh latar', 'MODIFY banner_services ( wh latar ) ', '122.102.40.66', '2016-04-21 10:48:13');
INSERT INTO `log` VALUES ('160', '1', 'UPDATE', 'banner_services', '17', 'wh banner1', 'MODIFY banner_services ( wh banner1 ) ', '122.102.40.66', '2016-04-21 10:48:18');
INSERT INTO `log` VALUES ('161', '1', 'ADD', 'message', '10', 'Rangga', 'ADDED message ( Rangga ) ', '122.102.40.66', '2016-04-22 13:43:52');
INSERT INTO `log` VALUES ('162', '1', 'ADD', 'message', '11', 'leni', 'ADDED message ( leni ) ', '122.102.40.66', '2016-04-22 13:45:41');
INSERT INTO `log` VALUES ('163', '1', 'ADD', 'message', '12', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 13:59:59');
INSERT INTO `log` VALUES ('164', '1', 'ADD', 'message', '13', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:05:29');
INSERT INTO `log` VALUES ('165', '1', 'ADD', 'message', '14', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:06:00');
INSERT INTO `log` VALUES ('166', '1', 'ADD', 'message', '15', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:06:44');
INSERT INTO `log` VALUES ('167', '1', 'ADD', 'message', '16', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:06:53');
INSERT INTO `log` VALUES ('168', '1', 'ADD', 'message', '17', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:09:29');
INSERT INTO `log` VALUES ('169', '1', 'ADD', 'message', '18', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:37:33');
INSERT INTO `log` VALUES ('170', '1', 'ADD', 'message', '19', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:38:08');
INSERT INTO `log` VALUES ('171', '1', 'ADD', 'message', '20', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:39:20');
INSERT INTO `log` VALUES ('172', '1', 'ADD', 'message', '21', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:42:41');
INSERT INTO `log` VALUES ('173', '1', 'ADD', 'message', '22', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:45:21');
INSERT INTO `log` VALUES ('174', '1', 'ADD', 'message', '23', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:47:45');
INSERT INTO `log` VALUES ('175', '1', 'ADD', 'message', '24', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 14:51:40');
INSERT INTO `log` VALUES ('176', '1', 'ADD', 'message', '25', 'Stevhanie Puspita Sari', 'ADDED message ( Stevhanie Puspita Sari ) ', '192.168.1.64', '2016-04-22 14:55:31');
INSERT INTO `log` VALUES ('177', '1', 'ADD', 'message', '26', 'Stevhanie Puspita Sari', 'ADDED message ( Stevhanie Puspita Sari ) ', '192.168.1.64', '2016-04-22 14:58:34');
INSERT INTO `log` VALUES ('178', '1', 'ADD', 'message', '27', 'Stevhanie Puspita Sari', 'ADDED message ( Stevhanie Puspita Sari ) ', '192.168.1.64', '2016-04-22 15:00:13');
INSERT INTO `log` VALUES ('179', '1', 'ADD', 'message', '28', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 15:01:13');
INSERT INTO `log` VALUES ('180', '1', 'ADD', 'message', '29', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 15:11:15');
INSERT INTO `log` VALUES ('181', '1', 'ADD', 'message', '30', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 15:15:15');
INSERT INTO `log` VALUES ('182', '1', 'ADD', 'message', '31', 'Leni', 'ADDED message ( Leni ) ', '122.102.40.66', '2016-04-22 15:24:11');
INSERT INTO `log` VALUES ('183', '1', 'ADD', 'message', '32', 'Ando Gositus', 'ADDED message ( Ando Gositus ) ', '122.102.40.66', '2016-04-22 15:25:54');
INSERT INTO `log` VALUES ('184', '1', 'ADD', 'message', '33', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 15:33:46');
INSERT INTO `log` VALUES ('185', '1', 'ADD', 'message', '34', 'Rangga Afriansyah', 'ADDED message ( Rangga Afriansyah ) ', '192.168.1.64', '2016-04-22 15:34:29');
INSERT INTO `log` VALUES ('186', '1', 'ADD', 'message', '35', 'Stevhanie Puspita Sari', 'ADDED message ( Stevhanie Puspita Sari ) ', '192.168.1.64', '2016-04-22 15:35:08');
INSERT INTO `log` VALUES ('187', '1', 'ADD', 'message', '36', 'Stevhanie Puspita Sari', 'ADDED message ( Stevhanie Puspita Sari ) ', '192.168.1.64', '2016-04-22 15:36:05');
INSERT INTO `log` VALUES ('188', '1', 'ADD', 'message', '37', 'Leni', 'ADDED message ( Leni ) ', '122.102.40.66', '2016-04-22 15:36:06');
INSERT INTO `log` VALUES ('189', '1', 'ADD', 'message', '38', 'Ando Gositus', 'ADDED message ( Ando Gositus ) ', '122.102.40.66', '2016-04-22 15:38:59');
INSERT INTO `log` VALUES ('190', '1', 'ADD', 'message', '39', 'bryando', 'ADDED message ( bryando ) ', '122.102.40.66', '2016-04-22 15:43:09');
INSERT INTO `log` VALUES ('191', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '202.57.4.4', '2016-04-22 20:50:04');
INSERT INTO `log` VALUES ('192', '1', 'ADD', 'message', '40', 'Leni', 'ADDED message ( Leni ) ', '202.57.4.4', '2016-04-22 20:50:36');
INSERT INTO `log` VALUES ('193', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.206', '2016-04-25 10:50:40');
INSERT INTO `log` VALUES ('194', '1', 'ADD', 'section', '1', 'Home >> About', 'ADDED section ( Home >> About ) ', '192.168.1.64', '2016-04-25 13:02:23');
INSERT INTO `log` VALUES ('195', '1', 'ADD', 'section', '2', 'Home >> Competitive Edge', 'ADDED section ( Home >> Competitive Edge ) ', '192.168.1.64', '2016-04-25 13:03:09');
INSERT INTO `log` VALUES ('196', '1', 'ADD', 'section', '3', 'Competitive Edge', 'ADDED section ( Competitive Edge ) ', '192.168.1.64', '2016-04-25 13:03:32');
INSERT INTO `log` VALUES ('197', '1', 'UPDATE', 'section', '3', 'Home >> Products', 'MODIFY section ( Home >> Products ) ', '192.168.1.64', '2016-04-25 13:04:07');
INSERT INTO `log` VALUES ('198', '1', 'ADD', 'section', '4', 'Home >> Service', 'ADDED section ( Home >> Service ) ', '192.168.1.64', '2016-04-25 13:04:38');
INSERT INTO `log` VALUES ('199', '1', 'ADD', 'section', '5', 'Home >> Tenant', 'ADDED section ( Home >> Tenant ) ', '192.168.1.64', '2016-04-25 13:05:00');
INSERT INTO `log` VALUES ('200', '1', 'UPDATE', 'module', '26', 'Networks', 'DEACTIVATE module ( Networks ) - Reason: Hide', '192.168.1.64', '2016-04-25 15:49:29');
INSERT INTO `log` VALUES ('201', '1', 'UPDATE', 'module', '26', 'Networks', 'ACTIVATE module ( Networks ) - Reason: aktif', '192.168.1.64', '2016-04-25 15:49:43');
INSERT INTO `log` VALUES ('202', '1', 'UPDATE', 'module', '30', 'Networks', 'DEACTIVATE module ( Networks ) - Reason: Disabled', '192.168.1.64', '2016-04-25 15:50:00');
INSERT INTO `log` VALUES ('203', '1', 'UPDATE', 'module', '9', 'Article', 'MODIFY module ( Article ) ', '192.168.1.64', '2016-04-25 16:04:54');
INSERT INTO `log` VALUES ('204', '1', 'UPDATE', 'module', '32', 'Industry Solution', 'DEACTIVATE module ( Industry Solution ) - Reason: Hide', '192.168.1.64', '2016-04-25 16:05:11');
INSERT INTO `log` VALUES ('205', '1', 'UPDATE', 'module', '31', 'Services', 'DEACTIVATE module ( Services ) - Reason: Hide', '192.168.1.64', '2016-04-25 16:05:18');
INSERT INTO `log` VALUES ('206', '1', 'ADD', 'article', '1', 'Home Landing text', 'ADDED article ( Home Landing text ) ', '192.168.1.64', '2016-04-26 10:24:50');
INSERT INTO `log` VALUES ('207', '1', 'ADD', 'article', '2', 'Competitive Edge Landing Text', 'ADDED article ( Competitive Edge Landing Text ) ', '192.168.1.64', '2016-04-26 10:27:39');
INSERT INTO `log` VALUES ('208', '1', 'ADD', 'article', '3', 'Strategic Location', 'ADDED article ( Strategic Location ) ', '192.168.1.64', '2016-04-26 10:28:05');
INSERT INTO `log` VALUES ('209', '1', 'UPDATE', 'article', '3', 'Products', 'MODIFY article ( Products ) ', '192.168.1.64', '2016-04-26 10:32:45');
INSERT INTO `log` VALUES ('210', '1', 'LOGIN', 'admin', '1', 'Administrator', 'Successful Login', '192.168.1.64', '2016-04-26 11:19:13');
INSERT INTO `log` VALUES ('211', '1', 'UPDATE', 'article', '3', 'Products', 'MODIFY article ( Products ) ', '192.168.1.64', '2016-04-26 11:22:12');
INSERT INTO `log` VALUES ('212', '1', 'ADD', 'article', '4', 'Tenant landing Text', 'ADDED article ( Tenant landing Text ) ', '192.168.1.64', '2016-04-26 11:30:57');
INSERT INTO `log` VALUES ('213', '1', 'UPDATE', 'section', '1', 'Home >> About >> Mission', 'MODIFY section ( Home >> About >> Mission ) ', '192.168.1.64', '2016-04-26 11:50:46');
INSERT INTO `log` VALUES ('214', '1', 'ADD', 'section', '6', 'Home >> About >> Vision', 'ADDED section ( Home >> About >> Vision ) ', '192.168.1.64', '2016-04-26 11:51:06');
INSERT INTO `log` VALUES ('215', '1', 'UPDATE', 'section', '2', 'Home >> About >> Vision', 'MODIFY section ( Home >> About >> Vision ) ', '192.168.1.64', '2016-04-26 11:51:47');
INSERT INTO `log` VALUES ('216', '1', 'UPDATE', 'section', '6', 'tEST', 'MODIFY section ( tEST ) ', '192.168.1.64', '2016-04-26 11:51:52');
INSERT INTO `log` VALUES ('217', '1', 'ADD', 'section', '7', 'Home >> About', 'ADDED section ( Home >> About ) ', '192.168.1.64', '2016-04-26 11:52:18');
INSERT INTO `log` VALUES ('218', '1', 'UPDATE', 'section', '7', 'Home', 'MODIFY section ( Home ) ', '192.168.1.64', '2016-04-26 11:52:25');
INSERT INTO `log` VALUES ('219', '1', 'UPDATE', 'section', '3', 'Home >> About', 'MODIFY section ( Home >> About ) ', '192.168.1.64', '2016-04-26 11:52:32');
INSERT INTO `log` VALUES ('220', '1', 'UPDATE', 'section', '4', 'Home >> Competitive Edge', 'MODIFY section ( Home >> Competitive Edge ) ', '192.168.1.64', '2016-04-26 11:53:02');
INSERT INTO `log` VALUES ('221', '1', 'UPDATE', 'section', '5', 'Home >> Competitive Edge >> List', 'MODIFY section ( Home >> Competitive Edge >> List ) ', '192.168.1.64', '2016-04-26 11:53:14');
INSERT INTO `log` VALUES ('222', '1', 'UPDATE', 'section', '3', 'Home >> About >> Mission', 'MODIFY section ( Home >> About >> Mission ) ', '192.168.1.64', '2016-04-26 11:53:35');
INSERT INTO `log` VALUES ('223', '1', 'UPDATE', 'section', '1', 'Home >> About', 'MODIFY section ( Home >> About ) ', '192.168.1.64', '2016-04-26 11:53:41');
INSERT INTO `log` VALUES ('224', '1', 'UPDATE', 'section', '1', 'About', 'MODIFY section ( About ) ', '192.168.1.64', '2016-04-26 11:53:58');
INSERT INTO `log` VALUES ('225', '1', 'UPDATE', 'section', '2', 'About >> Vision', 'MODIFY section ( About >> Vision ) ', '192.168.1.64', '2016-04-26 11:54:03');
INSERT INTO `log` VALUES ('226', '1', 'UPDATE', 'section', '3', 'About >> Mission', 'MODIFY section ( About >> Mission ) ', '192.168.1.64', '2016-04-26 11:54:08');
INSERT INTO `log` VALUES ('227', '1', 'UPDATE', 'section', '4', 'Competitive Edge', 'MODIFY section ( Competitive Edge ) ', '192.168.1.64', '2016-04-26 11:54:15');
INSERT INTO `log` VALUES ('228', '1', 'UPDATE', 'section', '5', 'Competitive Edge >> List', 'MODIFY section ( Competitive Edge >> List ) ', '192.168.1.64', '2016-04-26 11:54:19');
INSERT INTO `log` VALUES ('229', '1', 'UPDATE', 'article', '1', 'Mission', 'MODIFY article ( Mission ) ', '192.168.1.64', '2016-04-26 11:54:44');
INSERT INTO `log` VALUES ('230', '1', 'UPDATE', 'article', '2', 'Competitive Edge Landing Text', 'MODIFY article ( Competitive Edge Landing Text ) ', '192.168.1.64', '2016-04-26 11:54:59');
INSERT INTO `log` VALUES ('231', '1', 'UPDATE', 'section', '6', 'Products', 'MODIFY section ( Products ) ', '192.168.1.64', '2016-04-26 11:56:19');
INSERT INTO `log` VALUES ('232', '1', 'UPDATE', 'section', '7', 'Products >> List', 'MODIFY section ( Products >> List ) ', '192.168.1.64', '2016-04-26 11:56:28');
INSERT INTO `log` VALUES ('233', '1', 'ADD', 'section', '8', 'Services', 'ADDED section ( Services ) ', '192.168.1.64', '2016-04-26 11:57:37');
INSERT INTO `log` VALUES ('234', '1', 'ADD', 'section', '9', 'Services >> List', 'ADDED section ( Services >> List ) ', '192.168.1.64', '2016-04-26 11:58:00');
INSERT INTO `log` VALUES ('235', '1', 'UPDATE', 'article', '3', 'Residential Property', 'MODIFY article ( Residential Property ) ', '192.168.1.64', '2016-04-26 13:05:28');
INSERT INTO `log` VALUES ('236', '1', 'ADD', 'section', '10', 'Tenant', 'ADDED section ( Tenant ) ', '192.168.1.64', '2016-04-26 13:10:42');
INSERT INTO `log` VALUES ('237', '1', 'ADD', 'section', '11', 'Tenant >> List', 'ADDED section ( Tenant >> List ) ', '192.168.1.64', '2016-04-26 13:10:58');
INSERT INTO `log` VALUES ('238', '1', 'UPDATE', 'article', '4', 'Tenant landing Text', 'MODIFY article ( Tenant landing Text ) ', '192.168.1.64', '2016-04-26 13:11:42');
INSERT INTO `log` VALUES ('239', '1', 'UPDATE', 'article', '4', 'Tenant landing Text', 'MODIFY article ( Tenant landing Text ) ', '192.168.1.64', '2016-04-26 13:32:39');
INSERT INTO `log` VALUES ('240', '1', 'ADD', 'section', '12', 'Milestone', 'ADDED section ( Milestone ) ', '192.168.1.64', '2016-04-26 13:34:28');
INSERT INTO `log` VALUES ('241', '1', 'ADD', 'section', '13', 'Milestone >> List', 'ADDED section ( Milestone >> List ) ', '192.168.1.64', '2016-04-26 13:34:40');
INSERT INTO `log` VALUES ('242', '1', 'LOGOUT', 'admin', '1', 'Administrator', 'Logout', '122.102.40.66', '2016-04-26 15:35:37');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `message_address` text NOT NULL,
  `message_email` varchar(255) NOT NULL,
  `message_phone` varchar(255) NOT NULL,
  `message_company` varchar(255) NOT NULL,
  `message_subject` varchar(255) NOT NULL,
  `message_content` text NOT NULL,
  `message_reply` text NOT NULL,
  `message_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flag` tinyint(4) NOT NULL DEFAULT '2',
  `flag_memo` varchar(255) NOT NULL DEFAULT 'New Message',
  `replied` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `module_alias` varchar(100) NOT NULL,
  `module_parent` int(11) NOT NULL,
  `module_url` varchar(100) NOT NULL DEFAULT 'javascript:;',
  `module_notes` text NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '1', 'Setting', 'setting', '0', 'javascript:;', 'Hanya tampil untuk admin gositus', '1', '');
INSERT INTO `module` VALUES ('2', '2', 'Module', 'module', '1', 'module', '', '1', '');
INSERT INTO `module` VALUES ('3', '3', 'Language', 'language', '1', 'language', '', '1', '');
INSERT INTO `module` VALUES ('4', '4', 'Section', 'section', '1', 'section', '', '1', '');
INSERT INTO `module` VALUES ('5', '5', 'Website Management', 'website_management', '0', 'javascript:;', '', '1', '');
INSERT INTO `module` VALUES ('6', '6', 'Admin List', 'admin_list', '5', 'admin', '', '1', '');
INSERT INTO `module` VALUES ('7', '7', 'Settings', 'settings', '5', 'setting', '', '1', '');
INSERT INTO `module` VALUES ('8', '8', 'Content', 'content', '0', 'javascript:;', '', '1', '');
INSERT INTO `module` VALUES ('9', '9', 'Article', 'article', '8', 'article', '', '1', '');
INSERT INTO `module` VALUES ('10', '10', 'News & Event', 'news_event', '8', 'news', 'Penambahan news & events', '1', '');
INSERT INTO `module` VALUES ('11', '11', 'F.A.Q', 'f.a.q', '8', 'faq', 'Frequently Asked Questions', '2', '');
INSERT INTO `module` VALUES ('12', '12', 'Banner', 'banner', '8', 'banner', 'rolling banner', '1', '');
INSERT INTO `module` VALUES ('13', '13', 'Gallery', 'gallery', '0', 'javascript:;', 'untuk photo + video', '2', '');
INSERT INTO `module` VALUES ('14', '14', 'Category', 'category', '13', 'gallery_category', 'category untuk gallery (video + photo)', '1', '');
INSERT INTO `module` VALUES ('15', '15', 'Photo', 'photo', '13', 'photo', '', '1', '');
INSERT INTO `module` VALUES ('16', '16', 'Video', 'video', '13', 'video', 'youtube video link.', '1', '');
INSERT INTO `module` VALUES ('17', '17', 'Product', 'product', '0', 'javascript:;', '', '2', '');
INSERT INTO `module` VALUES ('18', '18', 'Category', 'category', '17', 'product_category', '', '1', '');
INSERT INTO `module` VALUES ('19', '19', 'Item', 'item', '17', 'item', 'product listing', '1', '');
INSERT INTO `module` VALUES ('20', '20', 'Testimonial', 'testimonial', '8', 'testimonial', '', '2', '');
INSERT INTO `module` VALUES ('21', '21', 'Message', 'message', '0', 'javascript:;', 'message dari contact us', '1', '');
INSERT INTO `module` VALUES ('22', '22', 'Inbox', 'inbox', '21', 'inbox', '', '1', '');
INSERT INTO `module` VALUES ('23', '23', 'Test Module', 'test_module', '8', 'test_module', '', '2', '');
INSERT INTO `module` VALUES ('24', '24', 'Compose', 'compose', '21', 'send_message', '', '2', '');
INSERT INTO `module` VALUES ('25', '25', 'Master Location', 'master_location', '30', 'location', '', '1', '');
INSERT INTO `module` VALUES ('26', '26', 'Networks', 'networks', '30', 'network', '', '1', 'aktif');
INSERT INTO `module` VALUES ('27', '27', 'Career', 'career', '0', 'javascript:;', '', '1', '');
INSERT INTO `module` VALUES ('28', '28', 'Job Category', 'job_category', '27', 'jobcategory', '', '1', '');
INSERT INTO `module` VALUES ('29', '29', 'Job Vacancy', 'job_vacancy', '27', 'jobvacancy', '', '1', '');
INSERT INTO `module` VALUES ('30', '30', 'Networks', 'networks', '0', 'javascript:;', '', '2', 'Disabled');
INSERT INTO `module` VALUES ('31', '31', 'Services', 'services', '8', 'services', '', '2', 'Hide');
INSERT INTO `module` VALUES ('32', '32', 'Industry Solution', 'industry_solution', '8', 'industry', '', '2', 'Hide');
INSERT INTO `module` VALUES ('33', '33', 'Services Banner', 'services_banner', '8', 'banner_services', '', '2', 'hide');

-- ----------------------------
-- Table structure for network
-- ----------------------------
DROP TABLE IF EXISTS `network`;
CREATE TABLE `network` (
  `network_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `network_name` varchar(255) NOT NULL,
  `network_description` varchar(255) NOT NULL,
  `network_address` varchar(255) NOT NULL,
  `network_city` varchar(50) NOT NULL,
  `network_postcode` varchar(20) NOT NULL,
  `network_phone` varchar(255) NOT NULL,
  `network_fax` varchar(20) NOT NULL,
  `network_location` int(11) NOT NULL,
  `network_gmap` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of network
-- ----------------------------

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `news_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `news_type` tinyint(4) NOT NULL,
  `news_image` varchar(255) NOT NULL,
  `news_content` text NOT NULL,
  `news_start` datetime NOT NULL,
  `news_end` datetime NOT NULL,
  `news_popular` tinyint(4) NOT NULL DEFAULT '1',
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------

-- ----------------------------
-- Table structure for photo
-- ----------------------------
DROP TABLE IF EXISTS `photo`;
CREATE TABLE `photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `photo_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `photo_category` int(11) NOT NULL,
  `photo_image` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of photo
-- ----------------------------

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_category_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `product_category_parent` int(11) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category
-- ----------------------------

-- ----------------------------
-- Table structure for section
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `section_name` varchar(100) NOT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', '1', 'About', '1', '');
INSERT INTO `section` VALUES ('2', '2', 'About >> Vision', '1', '');
INSERT INTO `section` VALUES ('3', '3', 'About >> Mission', '1', '');
INSERT INTO `section` VALUES ('4', '4', 'Competitive Edge', '1', '');
INSERT INTO `section` VALUES ('5', '5', 'Competitive Edge >> List', '1', '');
INSERT INTO `section` VALUES ('6', '6', 'Products', '1', '');
INSERT INTO `section` VALUES ('7', '7', 'Products >> List', '1', '');
INSERT INTO `section` VALUES ('8', '8', 'Services', '1', '');
INSERT INTO `section` VALUES ('9', '9', 'Services >> List', '1', '');
INSERT INTO `section` VALUES ('10', '10', 'Tenant', '1', '');
INSERT INTO `section` VALUES ('11', '11', 'Tenant >> List', '1', '');
INSERT INTO `section` VALUES ('12', '12', 'Milestone', '1', '');
INSERT INTO `section` VALUES ('13', '13', 'Milestone >> List', '1', '');

-- ----------------------------
-- Table structure for send_message
-- ----------------------------
DROP TABLE IF EXISTS `send_message`;
CREATE TABLE `send_message` (
  `send_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `send_message_from` int(11) NOT NULL,
  `send_message_to` varchar(255) NOT NULL,
  `send_message_cc` varchar(255) NOT NULL,
  `send_message_bcc` varchar(255) NOT NULL,
  `send_message_subject` varchar(255) NOT NULL,
  `send_message_content` varchar(255) NOT NULL,
  `send_message_attach` varchar(255) NOT NULL,
  `send_message_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`send_message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of send_message
-- ----------------------------

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `services_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `services_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `services_head` text NOT NULL,
  `services_image` varchar(255) NOT NULL,
  `services_image_content` varchar(255) DEFAULT NULL,
  `services_content` text NOT NULL,
  `services_section` int(11) NOT NULL,
  `services_link` varchar(200) DEFAULT NULL,
  `sort` int(4) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '1',
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`services_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of services
-- ----------------------------

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(100) NOT NULL,
  `setting_address` varchar(255) NOT NULL,
  `setting_working_time` varchar(255) NOT NULL,
  `setting_country` varchar(50) NOT NULL,
  `setting_city` varchar(50) NOT NULL,
  `setting_postcode` varchar(10) NOT NULL,
  `setting_phone` varchar(100) NOT NULL,
  `setting_mobile` varchar(100) NOT NULL,
  `setting_bb_pin` varchar(15) NOT NULL,
  `setting_fax` varchar(100) NOT NULL,
  `setting_email` varchar(100) NOT NULL,
  `setting_ym` varchar(100) NOT NULL,
  `setting_msn` varchar(100) NOT NULL,
  `setting_facebook` varchar(150) NOT NULL,
  `setting_twitter` varchar(50) NOT NULL,
  `setting_google_map` varchar(100) NOT NULL,
  `setting_google_analytics` varchar(100) NOT NULL,
  `setting_web_title` varchar(100) NOT NULL,
  `setting_web_motto` varchar(255) NOT NULL,
  `setting_web_logo` varchar(255) NOT NULL,
  `setting_favicon` varchar(255) NOT NULL,
  `setting_meta_desc` varchar(255) NOT NULL,
  `setting_meta_key` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES ('1', 'Go Online Solusi ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'logo-email.jpg', '', '', '', '2', '');
INSERT INTO `setting` VALUES ('2', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Quote of the Day', 'Quote of the Day', 'logo-email.jpg', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('6', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Australis Leisure', 'Quote of the Day', 'web-logo.png', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('7', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Australis Leisure', 'Quote of the Day', 'go-online-solusi1.png', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('8', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Quote of the Day', 'Quote of the Day', 'logo-email1.jpg', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('9', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', 'zgdgfdzg', '', '', '', '', '', '', 'Quote of the Day', 'Quote of the Day', 'logo-email1.jpg', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('10', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Quote of the Day', 'Quote of the Day', 'logo-email1.jpg', '', 'Quote of the Day', 'Quote of the Day', '2', '0');
INSERT INTO `setting` VALUES ('11', 'Quote of the Day', 'Quote of the Day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PT. Puninar Logistics', 'Quote of the Day', 'logo-email1.jpg', '', 'PT. Puninar Logistics', 'PT. Puninar Logistics', '2', '0');
INSERT INTO `setting` VALUES ('17', 'PANBIL', 'PANBIL', 'PANBIL', 'Indonesia', 'Jakarta', '13910', '+62 21 460 2278', '08123456785', 'AAAAAAA', '+62 21 460 4886', 'rangga@gositus.com', '', '', 'PANBIL', 'PANBIL', '', '', 'PANBIL', 'PANBIL', 'logo-email1.jpg', '', 'PANBIL', 'PANBIL', '2', '0');
INSERT INTO `setting` VALUES ('56', 'Panbil Industrial Estate', 'Panbil Plaza, Jl. Ahmad Yani, Muka Kuning', '-', 'Indonesia', 'Batam', '29433', '+62 778 371000', '08123456785', 'AAAAAAA', '+62 778 371100', 'agnes@panbil.co.id, ricky_rpj@panbil.co.id', '', '', '', '', '', '', 'Panbil Indutrial Estate', '-', 'logo-email1.jpg', '', 'PANBIL', 'PANBIL', '2', '0');
INSERT INTO `setting` VALUES ('57', 'Panbil Industrial Estate', 'Panbil Plaza, Jl. Ahmad Yani, Muka Kuning', '-', 'Indonesia', 'Batam', '29433', '+62 778 371000', '08123456785', 'AAAAAAA', '+62 778 371100', 'agnes@panbil.co.id, ricky_rpj@panbil.co.id', '', '', '', '', '', '', 'Panbil Indutrial Estate', '-', 'logo-email1.jpg', '', 'Panbil industrial estate is committed to providing world class manufacturing facilities, excellent infrastructure and fully integrated support services to the investors.', 'PANBIL, Industrial, Estate', '2', '0');
INSERT INTO `setting` VALUES ('58', 'Panbil Industrial Estate', 'Panbil Plaza, Jl. Ahmad Yani, Muka Kuning', '-', 'Indonesia', 'Batam', '29433', '+62 778 371000', '08123456785', 'AAAAAAA', '+62 778 371100', 'agnes@panbil.co.id, ricky_rpj@panbil.co.id', '', '', '', '', '', '', 'Panbil Indutrial Estate', '-', 'logo-email1.jpg', '', 'Panbil industrial estate is committed to providing world class manufacturing facilities, excellent infrastructure and fully integrated support services to the investors.', 'PANBIL, Industrial, Estate', '1', '0');

-- ----------------------------
-- Table structure for testimonial
-- ----------------------------
DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE `testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `testimonial_name` varchar(255) NOT NULL,
  `testimonial_content` text NOT NULL,
  `testimonial_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of testimonial
-- ----------------------------

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `video_category` int(11) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `flag_memo` varchar(255) NOT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of video
-- ----------------------------
