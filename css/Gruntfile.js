module.exports = function(grunt) {

    'use strict';

    var config = {
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                sourceMap: false,
                outputStyle: 'nested'
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'scss',
                    src: ['*.scss'],
                    dest: '',
                    ext: '.css'
                }]
            }
        },
        cssbeautifier : {
            files : ['style.css', 'style320.css', 'style480.css', 'style768.css', 'style1366.css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: ['style.css', 'style320.css', 'style480.css', 'style768.css', 'style1366.css', '../application/views/template/*.php']
                },
                options: {
                    watchTask: true,
                    proxy: 'lab.gositus.com/panbil'
                }
                //proxy:{
                //    target: 'lab.gositus.com/kubos'
                //},
                //watchOptions: {
                //    ignoreInitial: true,
                //}
            }
        },
        watch: {
            css: {
                files: ['scss/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false
                }
            },
            cssbeautifier: {
            	files: ['scss/style.scss'],
            	tasks: ['cssbeautifier'],
            	options: {
            		spawn: false
            	}
            }
        }
    };

    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-cssbeautifier');
    //grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-sass');

    // Register tasks
    grunt.initConfig(config);

    grunt.registerTask('default', ['sass', 'cssbeautifier','browserSync', 'watch']);

    // grunt.registerTask('dev', ['watch']);

};
