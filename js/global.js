$(document).ready(function(){
	$('.bgLoading').loadingAll();
	$('.bg-one, .itemBanner, .imgBanner, .bg-two, .bg-three, .bg-triangle, .bg-five, .bg-six, .bg-seven').setBgHeight();
});


$(window).resize(function(){
	$('.bg-one, .itemBanner, .imgBanner, .bg-two, .bg-three, .bg-triangle, .bg-five, .bg-six, .bg-seven').setBgHeight();

});


$(window).scroll(function(){
	$(window).fixedMenu();
});

//LOAD FUNCTION
$(window).load(function(){

	if ($(".bgLoading").is(":visible")) {
        $('.bgLoading').delay(300).fadeIn(500);
        $('.bgLoading .logoWait').delay(300).fadeIn(500);
        $(".bgLoading").delay(1000).slideUp(500);
        // $(".bgLoading .logoWait").fadeOut(500);
        // $(".bgLoading .percen").fadeOut(500);
    }

    //
    //Services page
    $( '.page-services .service-item' ).each( function()
    {
        var item   = $(this);
        var height = item.height();
        item.height( 'auto' );
        item.attr( 'data-height', item.height() );
        item.height( height );
    });

    $( '.page-services .service-item a' ).click( function()
    {
        var item   = $(this).parents( '.service-item' );
        var height = item.data( 'height' );

        if ( item.hasClass( 'active' )) {
            return false;
        }

        closeServicesItems();

        $( '.arrow-down', item ).velocity({
            opacity: 0,
            duration: 100,
            sequenceQueue: false
        });
        $( '.arrow-up', item ).velocity({
            opacity: 1,
            duration: 100,
            sequenceQueue: false
        });
        item.velocity({
            height: height+'px',
            duration: 600
        });
        item.addClass( 'active' );

        return false;
    });

    function closeServicesItems()
    {
        $( '.page-services .service-item.active' ).each( function()
        {
            var item = $(this);
            $( '.arrow-down', item ).velocity({
                opacity: 1,
                duration: 100,
                sequenceQueue: false
            });
            $( '.arrow-up', item ).velocity({
                opacity: 0,
                duration: 100,
                sequenceQueue: false
            });
            item.velocity({
                height: '154px',
                duration: 600,
                sequenceQueue: false
            });
            item.removeClass( 'active' );
        });
    }

    //
    //Product filter
    //
    if ( $( '.partners-list-main').length > 0 )
    {
        var $grid = $('.partners-list-main').isotope({
            itemSelector: '.partner-item'
        });
        //filter 
        $('.partners-list-nav a').click( function() 
        {
            $('.partners-list-nav a').removeClass( 'active' );
            $(this).addClass( 'active' );

            var filterValue = $(this).attr( 'data-filter' );
            if ( filterValue == 'all' ) {
                filterValue = '*';
            } else {
                filterValue = '.partner-' + filterValue;
            }
            $grid.isotope({ filter: filterValue });

            return false;
        });
    }

    //
    //Gallery photo
    if ( $( '.album-photo' ).length > 0 )
    {
        $( '.album-photo' ).flexslider({
            animation: "slide",
            manualControls: ".album-photo-nav a",
            useCSS: false
        });
    }

    //
    //Mobile menu
    //
    $( '.menu-mobile-toggle a' ).click( function()
    {
        $( '.menu-mobile' ).addClass( 'active' );
        return false;
    });
    $( '.menu-mobile-close' ).click( function()
    {
        $( '.menu-mobile' ).removeClass( 'active' );
        return false;
    })
});