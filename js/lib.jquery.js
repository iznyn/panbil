$.fn.setBgHeight = function(){
	var bgHeight = this;
	var window_height = $(window).height() + parseInt(150);

	$(bgHeight).css({'min-height':window_height});
}

$.fn.fixedMenu = function(){
	var el = this;
	var topPosition = $(window).scrollTop();

	// $(window).scroll(function(){
		if(topPosition > 200){
			TweenMax.to('.menuFix', 0.4,{
				css:{
					top: 0,
					display: 'block'
				},
				ease: Cubic.ease
			});

			TweenMax.to('header', 0.4,{
				css:{
					display: 'none'
				},
				ease: Cubic.ease
			});
		}else{
			TweenMax.to('.menuFix', 0.4,{
				css:{
					top: '-100px',
					display: 'none'
				},
				ease: Cubic.ease
			});
			TweenMax.to('header', 0.4,{
				css:{
					display: 'block'
				},
				ease: Cubic.ease
			});
		}
	// });
}


$.fn.loadingAll = function(){
	var loadingBar = this.find('.loadingBar'),
		percen     = this.find('.percen'),
		loadingCount = 0,		
		resultImg = $("body img").length;

	loadingProgress();

	function loadingProgress(){
		$("body img").bind('load', function(){
			if(loadingCount < resultImg){
				loadingCount++;
				var wait = (loadingCount / resultImg) * 100;
				loadingAnimate(wait);				
			}
		});
	}

	function loadingAnimate(wait){
		TweenMax.to(loadingBar, 0.2,{
			css: {
				width: wait + "%"
			}
		});
		percen.html(Math.ceil(wait) + " %");
		setTimeout(function(){}, 500);
	}
}